package nb.tendens.shared;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */


import java.io.IOException;
import java.util.Properties;

import org.apache.wicket.markup.IMarkupFragment;
import org.apache.wicket.markup.MarkupParser;
import org.apache.wicket.markup.MarkupResourceStream;
import org.apache.wicket.util.resource.ResourceStreamNotFoundException;
import org.apache.wicket.util.resource.StringResourceStream;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mockito;
import org.sql2o.GenericDatasource;
import org.sql2o.Sql2o;

import nb.tendens.application.charts.ChartManager;
import nb.tendens.application.dashboards.DashboardManager;
import nb.tendens.application.meters.MeterManager;
import nb.tendens.application.metrics.MetricManager;
import nb.tendens.infrastructure.configuration.Configuration;
import nb.tendens.interfaces.WicketApplication;

/**
 * AbstractTestCase.
 *
 * @author N. Bulthuis
 *
 */

public abstract class AbstractTestCase {

	protected DashboardManager dashboardManager;
	protected ChartManager chartManager;

	protected MetricManager metricManager;
	protected MeterManager meterManager;

	private final GenericDatasource ds = new GenericDatasource("jdbc:h2:mem:test;MODE=MYSQL;DB_CLOSE_DELAY=-1",
			new Properties());

	/**
	 * Wicket Tester.
	 */
	private WicketTester wicketTester;

	/**
	 * @return the wicketTester
	 */
	public final WicketTester getWicketTester() {
		return this.wicketTester;
	}

	/**
	 * Setup a test.
	 *
	 * @throws Exception
	 *             on a setup error.
	 */
	@Before
	public final void setUp() throws Exception {

		final Configuration testConfiguration = Mockito.mock(Configuration.class);

		Mockito.when(testConfiguration.getDataSource()).thenReturn(ds);

		final WicketApplication wicketApplication = new WicketApplication() {
			@Override
			public Configuration getConfiguration() {

				return testConfiguration;

			};
		};

		this.wicketTester = new WicketTester(wicketApplication);

		meterManager = wicketApplication.getManagerFactory().getMeterManager();
		metricManager = wicketApplication.getManagerFactory().getMetricManager();
		chartManager = wicketApplication.getManagerFactory().getChartManager();
		dashboardManager = wicketApplication.getManagerFactory().getDashboardManager();

		setupTest();

	}

	/**
	 * Setup the Test.
	 *
	 * @throws Exception
	 *             on any error.
	 */
	public void setupTest() throws Exception {

	}

	/**
	 * Teardown tests.
	 *
	 * @throws Exception
	 *             on an error.
	 */
	@After
	public final void tearDown() throws Exception {

		new Sql2o(ds).open().createQuery("DROP ALL OBJECTS").executeUpdate();

		this.wicketTester.destroy();
		this.wicketTester = null;
	}

	/**
	 * Convert the String markup to a MarkupFragment.
	 *
	 * @param markup
	 *            the markup to convert.
	 * @return the markup fragment.
	 */
	public IMarkupFragment toMarkupFragment(final String markup) {

		try {
			final MarkupResourceStream stream = new MarkupResourceStream(new StringResourceStream(markup));

			final MarkupParser markupParser = getWicketTester().getApplication().getMarkupSettings().getMarkupFactory()
					.newMarkupParser(stream);

			return markupParser.parse();
		} catch (IOException | ResourceStreamNotFoundException e) {
			throw new RuntimeException(e);
		}

	}

}
