package nb.tendens.interfaces.meters;

import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.util.tester.FormTester;
import org.fest.assertions.Assertions;
import org.junit.Test;

import nb.tendens.application.meters.Meter;
import nb.tendens.shared.AbstractTestCase;
import nb.tendens.shared.TestHelper;

/**
 * MeterListPageTeset.
 *
 * @author N. Bulthuis
 *
 */
public class MeterListPageTest extends AbstractTestCase {

	/**
	 * Basic Render Test.
	 */
	@Test
	public final void addMeterFormSubmitShouldTriggerErrorWhenMeterNameIsNull() {

		getWicketTester().startPage(MeterListPage.class);

		final FormTester formTester = getWicketTester().newFormTester("addMeterForm");
		formTester.submit();

		getWicketTester().assertErrorMessages("'name' is required.");
	}

	/**
	 * Basic Render Test.
	 */
	@Test
	public final void addMeterFormSubmitShouldTriggerSaveMeter() {

		getWicketTester().startPage(MeterListPage.class);

		Assertions.assertThat(meterManager.count()).isEqualTo(0L);

		final String meterName = "test-meter-name";

		final FormTester formTester = getWicketTester().newFormTester("addMeterForm");
		formTester.setValue("editMeter:form:name", meterName);
		formTester.submit();

		Assertions.assertThat(meterManager.count()).isEqualTo(1L);

		final Meter meter = meterManager.findById(1L);
		Assertions.assertThat(meter.getName()).isEqualTo(meterName);

	}

	/**
	 * Basic Render Test.
	 */
	@Test
	public final void basicRender() {

		getWicketTester().startPage(MeterListPage.class);
		getWicketTester().assertRenderedPage(MeterListPage.class);

	}

	public final void performFilterTest(final String filter, final int count) {

		getWicketTester().startPage(MeterListPage.class);

		final FormTester formTester = getWicketTester().newFormTester("filterForm");
		formTester.setValue("filter", filter);
		formTester.submit();

		final DefaultDataTable<Meter, String> table = (DefaultDataTable<Meter, String>) getWicketTester()
				.getComponentFromLastRenderedPage("filterForm:meterList:meterList");

		Assertions.assertThat(table.getItemCount()).isEqualTo(count);

	}

	@Test
	public final void testFilter() {

		final Meter meter = new Meter();
		// TestHelper.setProperty(meter, "id", 1L);
		TestHelper.setProperty(meter, "name", "meter-name");

		meterManager.saveOrUpdate(meter);

		performFilterTest("meter", 1);
		performFilterTest("fubar", 0);
	}

}
