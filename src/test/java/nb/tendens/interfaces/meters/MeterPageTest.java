package nb.tendens.interfaces.meters;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */


import org.apache.wicket.model.Model;
import org.apache.wicket.util.tester.FormTester;
import org.fest.assertions.Assertions;
import org.junit.Test;

import nb.tendens.application.meters.Meter;
import nb.tendens.shared.AbstractTestCase;
import nb.tendens.shared.TestHelper;

/**
 * MeterPageTest.
 *
 * @author N. Bulthuis
 *
 */
public class MeterPageTest extends AbstractTestCase {

	@Test
	public final void addMeterFormSubmitShouldTriggerSaveMeter() {

		final Meter meter = meterManager.findById(1L);

		getWicketTester().startPage(new MeterPage(Model.of(meter)));
		getWicketTester().assertRenderedPage(MeterPage.class);

		final String meterName = "test-meter-name";

		final FormTester formTester = getWicketTester().newFormTester("form");
		formTester.setValue("editMeter:form:name", meterName);
		formTester.submit();

		final Meter meterNew = meterManager.findById(1L);
		Assertions.assertThat(meterNew.getName()).isEqualTo(meterName);

	}

	/**
	 * Basic Render Test.
	 */
	@Test
	public final void basicRender() {

		final Meter meter = meterManager.findById(1L);

		getWicketTester().startPage(new MeterPage(Model.of(meter)));
		getWicketTester().assertRenderedPage(MeterPage.class);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.shared.AbstractTestCase#setupTest()
	 */
	@Override
	public void setupTest() throws Exception {

		final Meter m = new Meter();
		TestHelper.setProperty(m, "name", "meter");
		meterManager.saveOrUpdate(m);

		super.setupTest();
	}
}
