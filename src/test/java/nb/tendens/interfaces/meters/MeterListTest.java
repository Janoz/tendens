package nb.tendens.interfaces.meters;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */


import org.fest.assertions.Assertions;
import org.junit.Test;

import nb.tendens.application.meters.Meter;
import nb.tendens.shared.AbstractTestCase;
import nb.tendens.shared.TestHelper;

/**
 * MeterListTest.
 *
 * @author N. Bulthuis
 *
 */
public class MeterListTest extends AbstractTestCase {

	/**
	 * Basic Render Test.
	 */
	@Test
	public final void basicRender() {

		getWicketTester().startComponentInPage(MeterList.class);

	}

	/**
	 * Basic Render Test.
	 */
	@Test
	public final void deleteMeterShouldCallDeleteOnMeterManager() {

		final Meter meter = new Meter();
		// TestHelper.setProperty(meter, "id", 1L);
		TestHelper.setProperty(meter, "name", "meter-name");

		meterManager.saveOrUpdate(meter);

		Assertions.assertThat(meterManager.count()).isEqualTo(1L);

		getWicketTester().startComponentInPage(MeterList.class);
		getWicketTester().clickLink("testObject:meterList:body:rows:1:cells:3:cell:delete");

		Assertions.assertThat(meterManager.count()).isEqualTo(0L);

	}
}
