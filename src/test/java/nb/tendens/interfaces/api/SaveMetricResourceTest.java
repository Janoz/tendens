package nb.tendens.interfaces.api;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;

import nb.tendens.application.meters.Meter;
import nb.tendens.application.metrics.Metric;
import nb.tendens.shared.AbstractTestCase;
import nb.tendens.shared.TestHelper;

/**
 * APIResourceTest.
 *
 * @author N. Bulthuis
 *
 */
public class SaveMetricResourceTest extends AbstractTestCase {

	private static final Date DATE_20150101;
	static {
		Date d = null;
		try {
			 d = DateUtils.parseDate("2015-01-01T00:00:00+0200",
                            DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.getPattern());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		DATE_20150101 = d;
	}
	/**
	 * Test.
	 */
	@Test
	public void duplicateMetricShouldResultInUpdate() throws Exception {
		final Meter meter = meterManager.findById(1L);

		final Date dt = DATE_20150101;

		getWicketTester().getRequest().setMethod("POST");
		getWicketTester().getRequest().setParameter("id", "1");
		getWicketTester().getRequest().setParameter("dt", String.valueOf(dt.getTime()));
		getWicketTester().getRequest().setParameter("v", "1");

		getWicketTester().executeUrl("./api/metric");

		Assertions.assertThat(getWicketTester().getLastResponse().getStatus()).isEqualTo(200);
		final Metric metric = metricManager.subList(meter, 0, 1).get(0);

		final Date expectedDateTime = DATE_20150101;

		Assertions.assertThat(metric.getDateTime().getTime()).isEqualTo(expectedDateTime.getTime());
		Assertions.assertThat(metric.getValue()).isEqualTo(1L);

		getWicketTester().getRequest().initialize();
		getWicketTester().getRequest().setMethod("POST");
		getWicketTester().getRequest().setParameter("id", "1");
		getWicketTester().getRequest().setParameter("dt", String.valueOf(dt.getTime()));
		getWicketTester().getRequest().setParameter("v", "2");

		getWicketTester().executeUrl("./api/metric");

		Assertions.assertThat(getWicketTester().getLastResponse().getStatus()).isEqualTo(200);

		final Metric updatedMetric = metricManager.subList(meter, 0, 1).get(0);
		Assertions.assertThat(updatedMetric.getValue()).isEqualTo(2L);

	}

	/**
	 * Test.
	 */
	@Test
	public void invalidDateTimeShouldReturnBadRequest() {

		getWicketTester().getRequest().setParameter("id", "1");
		getWicketTester().getRequest().setParameter("dt", "invalid");
		getWicketTester().getRequest().setParameter("v", "2");

		getWicketTester().executeUrl("./api/metric");

		Assertions.assertThat(getWicketTester().getLastResponse().getStatus()).isEqualTo(400);
		Assertions.assertThat(getWicketTester().getLastResponse().getErrorMessage()).isEqualTo("Unable to convert 'invalid' to a long value");
	}

	/**
	 * Test.
	 */
	@Test
	public void meterDoesNotExistShouldReturnBadRequest() {

		getWicketTester().getRequest().setParameter("id", "2");
		getWicketTester().getRequest().setParameter("dt", String.valueOf(DATE_20150101.getTime()));
		getWicketTester().getRequest().setParameter("v", "2");

		getWicketTester().executeUrl("./api/metric");

		Assertions.assertThat(getWicketTester().getLastResponse().getStatus()).isEqualTo(400);
		Assertions.assertThat(getWicketTester().getLastResponse().getErrorMessage()).isEqualTo("meter matching 2 does not exist");

	}

	/**
	 * Test.
	 */
	@Test
	public void missingIdShouldReturnBadRequest() {

		// getWicketTester().getRequest().setParameter("id", "1");
		getWicketTester().getRequest().setParameter("dt", String.valueOf(DATE_20150101.getTime()));
		getWicketTester().getRequest().setParameter("v", "2");

		getWicketTester().executeUrl("./api/metric");

		Assertions.assertThat(getWicketTester().getLastResponse().getStatus()).isEqualTo(400);
		Assertions.assertThat(getWicketTester().getLastResponse().getErrorMessage()).isEqualTo("Field 'id' is null or empty");

	}

	/**
	 * Test.
	 */
	@Test
	public void missingValueShouldReturnBadRequest() {

		getWicketTester().getRequest().setParameter("id", "1");
		getWicketTester().getRequest().setParameter("dt", String.valueOf(DATE_20150101.getTime()));
		// getWicketTester().getRequest().setParameter("v", "2");

		getWicketTester().executeUrl("./api/metric");

		Assertions.assertThat(getWicketTester().getLastResponse().getStatus()).isEqualTo(400);
		Assertions.assertThat(getWicketTester().getLastResponse().getErrorMessage()).isEqualTo("Field 'v' is null or empty");

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.shared.AbstractMockTestCase#setupTest()
	 */
	@Override
	public void setupTest() throws Exception {
		super.setupTest();

		final Meter newMeter = new Meter();
		TestHelper.setProperty(newMeter, "name", "test-meter");

		meterManager.saveOrUpdate(newMeter);

	}

	/**
	 * Test.
	 */
	@Test
	public void whenDateIsNullThenTheMetricShouldBeSavedWithTheCurrentDate() throws Exception {
		final Meter meter = meterManager.findById(1L);

		getWicketTester().getRequest().setMethod("POST");
		getWicketTester().getRequest().setParameter("id", "1");
		// getWicketTester().getRequest().setParameter("dt",
		// "2015-01-01T00:00:00+0200");
		getWicketTester().getRequest().setParameter("v", "1");

		getWicketTester().executeUrl("./api/metric");

		Assertions.assertThat(getWicketTester().getLastResponse().getStatus()).isEqualTo(200);
		Assertions.assertThat(metricManager.subList(meter, 0, 1).size()).isEqualTo(1);

	}
}
