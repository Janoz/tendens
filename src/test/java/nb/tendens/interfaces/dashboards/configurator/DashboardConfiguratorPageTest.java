package nb.tendens.interfaces.dashboards.configurator;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.IOException;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.tester.FormTester;
import org.fest.assertions.Assertions;
import org.junit.Test;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.dashboards.Dashboard;
import nb.tendens.shared.AbstractTestCase;

/**
 * DashboardConfiguratorPageTest.
 *
 * @author N. Bulthuis
 *
 */
public class DashboardConfiguratorPageTest extends AbstractTestCase {

	/**
	 * Test.
	 */
	@Test
	public void basicRender() {
		getWicketTester().startPage(new DashboardConfiguratorPage(Model.of(Dashboard.newDefault())));
		getWicketTester().assertRenderedPage(DashboardConfiguratorPage.class);
	}

	/**
	 * Test.
	 */
	@Test
	public void saveDashboardTest() throws IOException {

		final IModel<Dashboard> model = Model.of(Dashboard.newDefault());

		getWicketTester().startPage(new DashboardConfiguratorPage(model));
		getWicketTester().assertRenderedPage(DashboardConfiguratorPage.class);

		final FormTester formTester = getWicketTester().newFormTester("form");

		final String name = "DashboardName";

		formTester.setValue("name", name);

		getWicketTester().executeAjaxEvent("form:addDashboardItem", "click");

		formTester.select("items:0:item:chartSelector", 0);
		getWicketTester().executeAjaxEvent("form:saveDashboard", "click");

		// formTester.submit();

		Assertions.assertThat(model.getObject().getName()).isEqualTo(name);
		Assertions.assertThat(model.getObject().getItems().size()).isEqualTo(1);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.shared.AbstractTestCase#setupTest()
	 */
	@Override
	public void setupTest() throws Exception {
		super.setupTest();

		final ChartConfiguration cc = ChartConfiguration.newDefault();
		cc.setName("ChartName");
		chartManager.saveOrUpdate(cc);
	}
}
