package nb.tendens.interfaces.charts;

import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.util.tester.FormTester;
import org.fest.assertions.Assertions;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.junit.Test;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.shared.AbstractTestCase;
import nb.tendens.shared.TestHelper;

/**
 * ChartListPageTest.
 *
 * @author N. Bulthuis
 *
 */
public class ChartListPageTest extends AbstractTestCase {

	/**
	 * Basic Render Test.
	 */
	@Test
	public final void basicRender() {

		getWicketTester().startPage(ChartListPage.class);
		getWicketTester().assertRenderedPage(ChartListPage.class);

	}

	public final void performFilterTest(final String filter, final int count) {

		getWicketTester().startPage(ChartListPage.class);

		getWicketTester().debugComponentTrees();

		final FormTester formTester = getWicketTester().newFormTester("form");
		formTester.setValue("filter", filter);
		formTester.submit();

		final DefaultDataTable<ChartConfiguration, String> table = (DefaultDataTable<ChartConfiguration, String>) getWicketTester()
				.getComponentFromLastRenderedPage("form:chartList:chartList");

		Assertions.assertThat(table.getItemCount()).isEqualTo(count);

	}

	@Test
	public final void testFilter() {

		final ChartConfiguration chart = ChartConfiguration.newDefault();
		// TestHelper.setProperty(meter, "id", 1L);
		TestHelper.setProperty(chart, "name", "chart-name");

		chartManager.saveOrUpdate(chart);

		performFilterTest("chart", 1);
		performFilterTest("fubar", 0);
	}

}
