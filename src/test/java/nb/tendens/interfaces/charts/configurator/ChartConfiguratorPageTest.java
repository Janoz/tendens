package nb.tendens.interfaces.charts.configurator;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.IOException;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.tester.FormTester;
import org.fest.assertions.Assertions;
import org.junit.Test;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.charts.DashStyle;
import nb.tendens.application.charts.PlotLine;
import nb.tendens.application.charts.datasources.Aggregator;
import nb.tendens.application.charts.datasources.Reductor;
import nb.tendens.application.charts.series.AreaRangeSerie;
import nb.tendens.application.charts.series.BasicSerie;
import nb.tendens.application.meters.Meter;
import nb.tendens.shared.AbstractTestCase;

/**
 * ChartConfiguratorPageTest.
 *
 * @author N. Bulthuis
 *
 */
public class ChartConfiguratorPageTest extends AbstractTestCase {

	/**
	 * Test.
	 */
	@Test
	public void basicRender() {
		getWicketTester().startPage(new ChartConfiguratorPage(Model.of(ChartConfiguration.newDefault())));
		getWicketTester().assertRenderedPage(ChartConfiguratorPage.class);
	}

	/**
	 * Test.
	 */
	@Test
	public void saveComplexChartTest() throws IOException {

		final IModel<ChartConfiguration> model = Model.of(ChartConfiguration.newDefault());

		getWicketTester().startPage(new ChartConfiguratorPage(model));
		getWicketTester().assertRenderedPage(ChartConfiguratorPage.class);

		final FormTester formTester = getWicketTester().newFormTester("form");

		final String name = "ChartName";
		final String from = "1 january 2015 at midnight";
		final String until = "today at midnight";
		final String labelFormat = "{value}";

		formTester.setValue("name", name);
		formTester.setValue("from", from);
		formTester.setValue("until", until);

		getWicketTester().executeAjaxEvent("form:addYAxis", "click");
		getWicketTester().executeAjaxEvent("form:yAxes:0:yAxis:addSerieAreaRange", "click");
		getWicketTester().executeAjaxEvent("form:yAxes:1:yAxis:addSerieLine", "click");
		getWicketTester().executeAjaxEvent("form:yAxes:1:yAxis:addSerieLine", "click");
		getWicketTester().executeAjaxEvent("form:yAxes:1:yAxis:addPlotLine", "click");

		final String arSerieName = "ARSerieName";
		final String color = "red";
		final String valueSuffix = "%";

		formTester.setValue("yAxes:0:yAxis:labelFormat", labelFormat);

		formTester.setValue("yAxes:0:yAxis:series:0:serie:common:name", arSerieName);
		formTester.setValue("yAxes:0:yAxis:series:0:serie:common:color", color);

		formTester.select("yAxes:0:yAxis:series:0:serie:dataSourceHigh:meter", 0);
		formTester.select("yAxes:0:yAxis:series:0:serie:dataSourceHigh:reductor", 1);
		formTester.select("yAxes:0:yAxis:series:0:serie:dataSourceHigh:aggregator", 2);

		formTester.select("yAxes:0:yAxis:series:0:serie:dataSourceLow:meter", 1);
		formTester.select("yAxes:0:yAxis:series:0:serie:dataSourceLow:reductor", 2);
		formTester.select("yAxes:0:yAxis:series:0:serie:dataSourceLow:aggregator", 3);

		final String defSerieName1 = "DefSerieName1";
		formTester.setValue("yAxes:1:yAxis:series:0:serie:common:name", defSerieName1);
		formTester.setValue("yAxes:1:yAxis:series:0:serie:common:color", color);
		formTester.select("yAxes:1:yAxis:series:0:serie:dataSource:meter", 2);
		formTester.select("yAxes:1:yAxis:series:0:serie:dataSource:reductor", 1);
		formTester.select("yAxes:1:yAxis:series:0:serie:dataSource:aggregator", 2);

		final String defSerieName2 = "DefSerieName2";
		formTester.setValue("yAxes:1:yAxis:series:1:serie:common:name", defSerieName2);
		formTester.setValue("yAxes:1:yAxis:series:1:serie:common:color", color);
		formTester.setValue("yAxes:1:yAxis:series:1:serie:common:valueSuffix", valueSuffix);
		formTester.select("yAxes:1:yAxis:series:1:serie:dataSource:meter", 1);
		formTester.select("yAxes:1:yAxis:series:1:serie:dataSource:reductor", 4);
		formTester.select("yAxes:1:yAxis:series:1:serie:dataSource:aggregator", 3);

		final String plotLineLabel = "Label";
		final String plotLineColor = "Color";
		final Double plotLineValue = 3.3D;

		formTester.setValue("yAxes:1:yAxis:plotLines:0:plotLine:label", plotLineLabel);
		formTester.setValue("yAxes:1:yAxis:plotLines:0:plotLine:color", plotLineColor);
		formTester.setValue("yAxes:1:yAxis:plotLines:0:plotLine:value", String.valueOf(plotLineValue));
		formTester.select("yAxes:1:yAxis:plotLines:0:plotLine:dashStyle", 0);

		getWicketTester().executeAjaxEvent("form:saveChart", "click");

		formTester.submit();

		// getWicketTester().executeAjaxEvent("form:redrawChart", "click");

		Assertions.assertThat(meterManager.count()).isEqualTo(3L);

		Assertions.assertThat(model.getObject().getName()).isEqualTo(name);
		Assertions.assertThat(model.getObject().getFrom()).isEqualTo(from);
		Assertions.assertThat(model.getObject().getUntil()).isEqualTo(until);

		Assertions.assertThat(model.getObject().getYAxes().size()).isEqualTo(2);
		getWicketTester().debugComponentTrees();

		Assertions.assertThat(model.getObject().getYAxes().get(0).getLabelFormat()).isEqualTo(labelFormat);
		Assertions.assertThat(model.getObject().getYAxes().get(0).getSeries().size()).isEqualTo(1);
		Assertions.assertThat(model.getObject().getYAxes().get(0).getSeries().get(0).getName()).isEqualTo(arSerieName);

		final AreaRangeSerie arSerie = (AreaRangeSerie) model.getObject().getYAxes().get(0).getSeries().get(0);

		Assertions.assertThat(arSerie.getName()).isEqualTo(arSerieName);
		Assertions.assertThat(arSerie.getColor()).isEqualTo(color);
		Assertions.assertThat(arSerie.getDataSourceHigh().getMeter().getName()).isEqualTo("M1");
		Assertions.assertThat(arSerie.getDataSourceHigh().getReductor()).isEqualTo(Reductor.MINUTE);
		Assertions.assertThat(arSerie.getDataSourceHigh().getAggregator()).isEqualTo(Aggregator.MAX);
		Assertions.assertThat(arSerie.getDataSourceLow().getMeter().getName()).isEqualTo("M2");
		Assertions.assertThat(arSerie.getDataSourceLow().getReductor()).isEqualTo(Reductor.MINUTES_5);
		Assertions.assertThat(arSerie.getDataSourceLow().getAggregator()).isEqualTo(Aggregator.MIN);

		final BasicSerie defSerie1 = (BasicSerie) model.getObject().getYAxes().get(1).getSeries().get(0);
		Assertions.assertThat(defSerie1.getName()).isEqualTo(defSerieName1);
		Assertions.assertThat(defSerie1.getColor()).isEqualTo(color);
		Assertions.assertThat(defSerie1.getDataSource().getMeter().getName()).isEqualTo("M3");
		Assertions.assertThat(defSerie1.getDataSource().getReductor()).isEqualTo(Reductor.MINUTE);
		Assertions.assertThat(defSerie1.getDataSource().getAggregator()).isEqualTo(Aggregator.MAX);

		final BasicSerie defSerie2 = (BasicSerie) model.getObject().getYAxes().get(1).getSeries().get(1);
		Assertions.assertThat(defSerie2.getName()).isEqualTo(defSerieName2);
		Assertions.assertThat(defSerie2.getColor()).isEqualTo(color);
		Assertions.assertThat(defSerie2.getValueSuffix()).isEqualTo(valueSuffix);
		Assertions.assertThat(defSerie2.getDataSource().getMeter().getName()).isEqualTo("M2");
		Assertions.assertThat(defSerie2.getDataSource().getReductor()).isEqualTo(Reductor.MINUTES_15);
		Assertions.assertThat(defSerie2.getDataSource().getAggregator()).isEqualTo(Aggregator.MIN);

		final PlotLine plotLine = model.getObject().getYAxes().get(1).getPlotLines().get(0);
		Assertions.assertThat(plotLine.getLabel()).isEqualTo(plotLineLabel);
		Assertions.assertThat(plotLine.getColor()).isEqualTo(plotLineColor);
		Assertions.assertThat(plotLine.getValue()).isEqualTo(plotLineValue);
		Assertions.assertThat(plotLine.getDashStyle()).isEqualTo(DashStyle.SOLID);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.shared.AbstractTestCase#setupTest()
	 */
	@Override
	public void setupTest() throws Exception {
		super.setupTest();

		final Meter m1 = new Meter();
		m1.setName("M1");
		meterManager.saveOrUpdate(m1);

		final Meter m2 = new Meter();
		m2.setName("M2");
		meterManager.saveOrUpdate(m2);
		final Meter m3 = new Meter();
		m3.setName("M3");
		meterManager.saveOrUpdate(m3);

	}

}
