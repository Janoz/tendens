package nb.tendens.domain.shared;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import static org.fest.assertions.Assertions.assertThat;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import nb.tendens.application.shared.AbstractPersistentObject;
import nb.tendens.shared.TestHelper;

/**
 * Tests for the PersistentObject .equals() and .hashcode() implementation.
 *
 * @author N. Bulthuis
 *
 * @since 1.0
 */
public final class PersistentObjectTest {

	/**
	 * PersistentObject with a non-null id.
	 */
	private final DummyPersistentObject o1 = new DummyPersistentObject();

	/**
	 * PersistentObject with a non-null id.
	 */
	private final DummyPersistentObject o2 = new DummyPersistentObject();

	/**
	 * Tests if two different PersistentObjects are not equal.
	 */
	@Test
	public void differentPersistentObjectsShouldNotBeEqual() {

		assertThat(o1).isNotEqualTo(o2);

	}

	/**
	 * Tests if two different PersistentObjects are not equal.
	 */
	@Test
	public void differentPersistentObjectsShouldNotBeEqualWithId() {

		TestHelper.setProperty(o1, "id", 1L);
		TestHelper.setProperty(o2, "id", 2L);

		assertThat(o1).isNotEqualTo(o2);

	}

	/**
	 * Tests if duplicate PersistentObjects cannot be in the same set.
	 */
	@Test
	public void duplicatePersistentObjectShouldNotBeInASet() {

		final Set<AbstractPersistentObject> set = new HashSet<AbstractPersistentObject>();
		set.add(o1);
		set.add(o1);

		assertThat(set.size()).isEqualTo(1);
		assertThat(set).containsOnly(o1);

	}

	/**
	 * Tests if a PersistentObject is not equal to null.
	 */
	@Test
	public void persistentObjectShouldNotBeEqualToNull() {

		assertThat(o1).isNotEqualTo(null);

	}

	/**
	 * Tests if a PersistentObject is not equal to null.
	 */
	@Test
	public void persistentObjectShouldNotBeEqualToNullWithId() {

		final DummyPersistentObject d1 = new DummyPersistentObject();
		TestHelper.setProperty(d1, "id", 1L);

		assertThat(d1).isNotEqualTo(null);

	}

	/**
	 * Tests if a PersistentObject is not equal to another type.
	 */
	@Test
	public void persistentObjectShouldNotBeEqualWithAnotherType() {

		assertThat(o1).isNotEqualTo("");

	}

	/**
	 * Tests if a PersistentObject is not equal to null.
	 */
	@Test
	public void persistentObjectShouldNotBeEqualWithAnotherTypeWithId() {

		final DummyPersistentObject d1 = new DummyPersistentObject();
		TestHelper.setProperty(d1, "id", 1L);

		assertThat(d1).isNotEqualTo("");

	}

	/**
	 * Tests if the same PersistentObjects are equal.
	 */
	@Test
	public void theSamePersistentObjectsShouldBeEqual() {

		assertThat(o1).isEqualTo(o1);

	}

}
