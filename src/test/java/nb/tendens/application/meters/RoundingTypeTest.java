package nb.tendens.application.meters;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;

import nb.tendens.application.shared.RoundingType;

/**
 * RoundingTypeTest.
 *
 * @author N. Bulthuis
 *
 */
public class RoundingTypeTest {

	/**
	 * Test 1 second boundary.
	 *
	 * @throws ParseException
	 *             on a parsing exception.
	 */
	@Test
	public final void assert1() throws ParseException {

		assertRounding(RoundingType.ROUND_AUTO, 1, "2015-01-01T13:11:45", "2015-01-01T13:11:45");
		assertRounding(RoundingType.ROUND_AUTO, 1, "2015-01-01T13:18:45", "2015-01-01T13:18:45");
		assertRounding(RoundingType.ROUND_DOWN, 1, "2015-01-01T13:11:45", "2015-01-01T13:11:45");
		assertRounding(RoundingType.ROUND_UP, 1, "2015-01-01T13:11:45", "2015-01-01T13:11:45");
	}

	/**
	 * Test 5 minute boundary.
	 *
	 * @throws ParseException
	 *             on a parsing exception.
	 */
	@Test
	public final void assert300() throws ParseException {

		assertRounding(RoundingType.ROUND_AUTO, 300, "2015-01-01T13:11:45", "2015-01-01T13:10:00");
		assertRounding(RoundingType.ROUND_AUTO, 300, "2015-01-01T13:18:45", "2015-01-01T13:20:00");
		assertRounding(RoundingType.ROUND_DOWN, 300, "2015-01-01T13:11:45", "2015-01-01T13:10:00");
		assertRounding(RoundingType.ROUND_UP, 300, "2015-01-01T13:11:45", "2015-01-01T13:15:00");
	}

	/**
	 * Test 1 minute boundary.
	 *
	 * @throws ParseException
	 *             on a parsing exception.
	 */
	@Test
	public final void assert60() throws ParseException {

		assertRounding(RoundingType.ROUND_AUTO, 60, "2015-01-01T13:11:45", "2015-01-01T13:12:00");
		assertRounding(RoundingType.ROUND_AUTO, 60, "2015-01-01T13:18:45", "2015-01-01T13:19:00");
		assertRounding(RoundingType.ROUND_DOWN, 60, "2015-01-01T13:11:45", "2015-01-01T13:11:00");
		assertRounding(RoundingType.ROUND_UP, 60, "2015-01-01T13:11:45", "2015-01-01T13:12:00");
	}

	/**
	 * Test 10 minute boundary.
	 *
	 * @throws ParseException
	 *             on a parsing exception.
	 */
	@Test
	public final void assert600() throws ParseException {

		assertRounding(RoundingType.ROUND_AUTO, 600, "2015-01-01T13:11:45", "2015-01-01T13:10:00");
		assertRounding(RoundingType.ROUND_AUTO, 600, "2015-01-01T13:18:45", "2015-01-01T13:20:00");
		assertRounding(RoundingType.ROUND_DOWN, 600, "2015-01-01T13:11:45", "2015-01-01T13:10:00");
		assertRounding(RoundingType.ROUND_UP, 600, "2015-01-01T13:11:45", "2015-01-01T13:20:00");
	}

	/**
	 * Assert Rounding.
	 *
	 * @param roundingType
	 *            the rounding type.
	 * @param roundingCount
	 *            the rounding count.
	 * @param input
	 *            the date input.
	 * @param expected
	 *            the date expected.
	 * @throws ParseException
	 *             on a parsing exception.
	 */
	public void assertRounding(final RoundingType roundingType, final int roundingCount, final String input,
			final String expected) throws ParseException {
		final Date inputDate = DateUtils.parseDate(input, DateFormatUtils.ISO_DATETIME_FORMAT.getPattern());

		final Date expectedDate = DateUtils.parseDate(expected, DateFormatUtils.ISO_DATETIME_FORMAT.getPattern());

		final Date resultDate = RoundingType.roundDateTime(roundingType, inputDate, roundingCount);
		Assertions.assertThat(resultDate).isEqualTo(expectedDate);
	}

}
