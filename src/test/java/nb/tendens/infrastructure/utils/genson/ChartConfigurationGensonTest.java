package nb.tendens.infrastructure.utils.genson;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.fest.assertions.Assertions;
import org.junit.Test;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.charts.YAxis;
import nb.tendens.application.charts.datasources.Aggregator;
import nb.tendens.application.charts.datasources.DataSource;
import nb.tendens.application.charts.datasources.Reductor;
import nb.tendens.application.charts.series.AbstractSerie;
import nb.tendens.application.charts.series.AreaRangeSerie;
import nb.tendens.application.charts.series.BasicSerie;
import nb.tendens.application.charts.series.SerieType;
import nb.tendens.infrastructure.utils.JsonUtilities;

/**
 * ChartConfigurationGsonTest.
 *
 * @author N. Bulthuis
 *
 */
public class ChartConfigurationGensonTest {

	/**
	 * Assert DataSource.
	 *
	 * @param expectedDS
	 * @param resultDS
	 */
	private final void assertDataSource(final DataSource expectedDS, final DataSource resultDS) {

		Assertions.assertThat(resultDS.getMeter()).isEqualTo(expectedDS.getMeter());

		Assertions.assertThat(resultDS.getAggregator()).isEqualTo(expectedDS.getAggregator());
		Assertions.assertThat(resultDS.getReductor()).isEqualTo(expectedDS.getReductor());

	}

	/**
	 * Assert Serie.
	 *
	 * @param expectedSerie
	 * @param resultSerie
	 */
	private void assertSerie(final AbstractSerie expectedSerie, final AbstractSerie resultSerie) {
		Assertions.assertThat(resultSerie).isInstanceOf(expectedSerie.getClass());
		Assertions.assertThat(resultSerie.getName()).isEqualTo(expectedSerie.getName());
		Assertions.assertThat(resultSerie.getColor()).isEqualTo(expectedSerie.getColor());

		if (expectedSerie instanceof BasicSerie) {
			final BasicSerie expectedSerieD = (BasicSerie) expectedSerie;
			final BasicSerie resultSerieD = (BasicSerie) resultSerie;

			assertDataSource(expectedSerieD.getDataSource(), resultSerieD.getDataSource());

		} else if (expectedSerie instanceof AreaRangeSerie) {
			final AreaRangeSerie expectedSerieD = (AreaRangeSerie) expectedSerie;
			final AreaRangeSerie resultSerieD = (AreaRangeSerie) resultSerie;

			assertDataSource(expectedSerieD.getDataSourceHigh(), resultSerieD.getDataSourceHigh());
			assertDataSource(expectedSerieD.getDataSourceLow(), resultSerieD.getDataSourceLow());
		}

	}

	/**
	 * Assert YAxis
	 *
	 * @param expectedYAxis
	 * @param resultYAxis
	 */
	private void assertYAxis(final YAxis expectedYAxis, final YAxis resultYAxis) {
		Assertions.assertThat(resultYAxis.getSeries().size()).isEqualTo(expectedYAxis.getSeries().size());

		for (int serieIndex = 0; serieIndex < expectedYAxis.getSeries().size(); serieIndex++) {
			final AbstractSerie expectedSerie = expectedYAxis.getSeries().get(serieIndex);
			final AbstractSerie resultSerie = resultYAxis.getSeries().get(serieIndex);

			assertSerie(expectedSerie, resultSerie);
		}

	}

	@Test
	public void testGsonSerialization() {

		final ChartConfiguration expected = new ChartConfiguration();
		expected.setName("ChartName");
		expected.setFrom("From");
		expected.setUntil("Until");

		final BasicSerie defSerie = new BasicSerie(SerieType.LINE);
		defSerie.setName("DefaultSerie");
		final AreaRangeSerie arSerie = new AreaRangeSerie(SerieType.AREARANGE);
		arSerie.setName("AreaRangeSerie");

		final DataSource defDS = defSerie.getDataSource();
		defDS.setAggregator(Aggregator.SUM);
		defDS.setReductor(Reductor.DAY);

		final DataSource arHigh = arSerie.getDataSourceHigh();
		arHigh.setAggregator(Aggregator.MAX);
		arHigh.setReductor(Reductor.MONTH);

		final DataSource arLow = arSerie.getDataSourceLow();

		expected.addYAxis().addSerie(defSerie);
		expected.addYAxis().addSerie(arSerie);

		final String json = JsonUtilities.newRuntimetypeGenson().serialize(expected);

		System.out.println(json);

		final ChartConfiguration result = JsonUtilities.newRuntimetypeGenson().deserialize(json,
				ChartConfiguration.class);

		Assertions.assertThat(result.getName()).isEqualTo(expected.getName());
		Assertions.assertThat(result.getFrom()).isEqualTo(expected.getFrom());
		Assertions.assertThat(result.getUntil()).isEqualTo(expected.getUntil());

		Assertions.assertThat(result.getYAxes().size()).isEqualTo(expected.getYAxes().size());

		for (int yAxisIndex = 0; yAxisIndex < expected.getYAxes().size(); yAxisIndex++) {

			final YAxis expectedYAxis = expected.getYAxes().get(yAxisIndex);
			final YAxis resultYAxis = result.getYAxes().get(yAxisIndex);

			assertYAxis(expectedYAxis, resultYAxis);
		}
	}

}
