package nb.tendens.interfaces.components.models;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.interfaces.WicketApplication;

/**
 * ChartLDM.
 *
 * @author N. Bulthuis
 *
 */
public class ChartLDM extends LoadableDetachableModel<ChartConfiguration> {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ChartLDM.class);

	/**
	 * Id.
	 */
	private final IModel<Long> id;

	/**
	 * Constructor.
	 *
	 * @param chart
	 *            chart.
	 */
	public ChartLDM(final ChartConfiguration chart) {
		this(chart.getId());
		setObject(chart);
	}

	public ChartLDM(final IModel<Long> chartId) {
		id = chartId;
	}

	/**
	 * Constructor.
	 *
	 * @param chartId
	 *            chartId.
	 */
	public ChartLDM(final Long chartId) {
		this(Model.of(chartId));

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final ChartConfiguration load() {

		LOG.info("Loading Chart: {}", id);
		final ChartConfiguration meter = WicketApplication.get().getManagerFactory().getChartManager()
				.findById(id.getObject());
		return meter;
	}

}
