package nb.tendens.interfaces.components.dataproviders;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nb.tendens.application.dashboards.Dashboard;
import nb.tendens.interfaces.WicketApplication;

/**
 * DashboardDataProvider.
 *
 * @author N. Bulthuis
 *
 */
public class DashboardDataProvider extends SortableDataProvider<Dashboard, String> {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DashboardDataProvider.class);

	/**
	 * Filter.
	 */
	private final IModel<String> filter;

	/**
	 * Constructor.
	 */
	public DashboardDataProvider(final IModel<String> filter) {
		super();

		this.filter = filter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void detach() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final Iterator<? extends Dashboard> iterator(final long first, final long count) {

		final List<Dashboard> meters = WicketApplication.get().getManagerFactory().getDashboardManager()
				.subList(filter.getObject(), first, count);
		return meters.iterator();

	};

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final IModel<Dashboard> model(final Dashboard dashboard) {
		return Model.of(dashboard);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long size() {

		final long count = WicketApplication.get().getManagerFactory().getDashboardManager().count(filter.getObject());
		return count;
	}
}
