package nb.tendens.interfaces.components.dataproviders;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nb.tendens.application.meters.Meter;
import nb.tendens.application.metrics.Metric;
import nb.tendens.interfaces.WicketApplication;
import nb.tendens.interfaces.components.models.MetricLDM;

/**
 * MetricDataProvider.
 *
 * @author N. Bulthuis
 *
 */
public class MetricDataProvider extends SortableDataProvider<Metric, String> {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MetricDataProvider.class);

	/**
	 * Meter Model.
	 */
	private final IModel<Meter> model;

	/**
	 * Constructor.
	 */
	public MetricDataProvider(final IModel<Meter> model) {
		super();

		this.model = model;

		setSort(new SortParam<String>("dateTime", false));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void detach() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final Iterator<? extends Metric> iterator(final long first, final long count) {
		final List<Metric> metrics = WicketApplication.get().getManagerFactory().getMetricManager()
				.subList(model.getObject(), first, count);
		return metrics.iterator();

	};

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final IModel<Metric> model(final Metric metric) {
		return new MetricLDM(metric);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long size() {

		final long count = WicketApplication.get().getManagerFactory().getMetricManager().count(model.getObject());
		return count;
	}
}
