package nb.tendens.interfaces.components.choicerenderers;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.List;

import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.model.IModel;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.interfaces.components.models.ChartListLDM;

/**
 * ChartIdChoiceRenderer.
 *
 * @author N. Bulthuis
 *
 */
public class ChartIdChoiceRenderer extends ChoiceRenderer<Long> {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Charts.
	 */
	private final ChartListLDM charts = new ChartListLDM();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final Object getDisplayValue(final Long id) {

		final List<ChartConfiguration> ccs = charts.getObject();

		for (final ChartConfiguration cc : ccs) {
			if (cc.getId() == id) {
				return cc.getName();
			}
		}

		return "";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.apache.wicket.markup.html.form.ChoiceRenderer#getIdValue(java.lang.
	 * Object, int)
	 */
	@Override
	public String getIdValue(final Long id, final int index) {

		return String.valueOf(id);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.apache.wicket.markup.html.form.ChoiceRenderer#getObject(java.lang.
	 * String, org.apache.wicket.model.IModel)
	 */
	@Override
	public Long getObject(final String id, final IModel<? extends List<? extends Long>> choices) {
		return super.getObject(id, choices);
	}

}
