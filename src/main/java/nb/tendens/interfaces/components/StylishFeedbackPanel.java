package nb.tendens.interfaces.components;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

/**
 * StylishFeedbackPanel.
 *
 * @author N. Bulthuis
 *
 */
public class StylishFeedbackPanel extends FeedbackPanel {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a new FeedbackPanel.
	 *
	 * @param id
	 *            the markup id.
	 */
	public StylishFeedbackPanel(final String id) {
		super(id);
	}

	/**
	 * Create a new FeedbackPanel with the specified message filter.
	 *
	 * @param id
	 *            markup id.
	 * @param filter
	 *            the message filter.
	 */
	public StylishFeedbackPanel(final String id, final IFeedbackMessageFilter filter) {
		super(id, filter);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.apache.wicket.markup.html.panel.FeedbackPanel#getCSSClass(org.apache.
	 * wicket.feedback.FeedbackMessage)
	 */
	@Override
	protected final String getCSSClass(final FeedbackMessage message) {
		String cssClass;

		// .error, .alert, .notice, .success, .info

		switch (message.getLevel()) {

			case FeedbackMessage.SUCCESS:
				cssClass = "alert-success";
				break;
			case FeedbackMessage.WARNING:
				cssClass = "alert-warning";
				break;
			case FeedbackMessage.ERROR:
			case FeedbackMessage.FATAL:
				cssClass = "alert-danger";
				break;
			case FeedbackMessage.DEBUG:
			case FeedbackMessage.INFO:
			case FeedbackMessage.UNDEFINED:
			default:
				cssClass = "alert-info";
		}
		return cssClass;
	}

}
