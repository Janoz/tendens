package nb.tendens.interfaces.charts.configurator.series;

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.markup.html.basic.Label;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.IModel;

import nb.tendens.application.charts.YAxis;
import nb.tendens.application.charts.series.AbstractSerie;

/**
 * AbstractSerieConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class AbstractSerieConfigurator extends Panel {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * YAxis.
	 */
	protected final IModel<YAxis> yAxis;

	/**
	 * YAxis.
	 */
	protected final IModel<AbstractSerie> serie;

	/**
	 * @param id
	 * @param model
	 */
	public AbstractSerieConfigurator(final String id, final IModel<YAxis> yAxis, final IModel<AbstractSerie> serie) {
		super(id, serie);

		this.serie = serie;
		this.yAxis = yAxis;
	}

	/**
	 * Add new Heading Label.
	 *
	 * @param markupId
	 *            the markup id.
	 * @return the label.
	 */
	private Label newHeadingLabel(final String markupId) {
		return new Label(markupId, new AbstractReadOnlyModel<String>() {
			/**
			 *
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public String getObject() {

				final String key = "serie.type." + StringUtils.lowerCase(serie.getObject().getSerieType().toString());
				final String localizedModelString = getLocalizer().getString(key, AbstractSerieConfigurator.this);
				return localizedModelString;
			}
		});
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();

		add(new CommonSerieConfigurator("common", yAxis, serie));

		add(new DeleteSerieLink("deleteSerie", yAxis, serie));

		add(newHeadingLabel("heading"));
	}
}
