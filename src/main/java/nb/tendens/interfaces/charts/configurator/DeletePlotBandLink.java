package nb.tendens.interfaces.charts.configurator;

import org.apache.wicket.AttributeModifier;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.StringResourceModel;

import nb.tendens.application.charts.PlotBand;
import nb.tendens.application.charts.XAxis;
import nb.tendens.interfaces.components.DefaultAjaxSubmitLink;

/**
 * DeletePlotBandLink.
 *
 * @author N. Bulthuis
 *
 */
public class DeletePlotBandLink extends DefaultAjaxSubmitLink {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * PlotBand.
	 */
	private final IModel<PlotBand> plotBand;

	/**
	 * XAxis.
	 */
	private final IModel<XAxis> xAxis;

	/**
	 * Constructor.
	 *
	 * @param markupId
	 *            the markup.
	 */
	public DeletePlotBandLink(final String markupId, final IModel<XAxis> xAxis, final IModel<PlotBand> plotBand) {
		super(markupId);

		this.plotBand = plotBand;
		this.xAxis = xAxis;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();
		add(AttributeModifier.append("title", new StringResourceModel("tooltip.delete.plotBand", this, plotBand)));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink#onSubmit(org.
	 * apache.wicket.ajax.AjaxRequestTarget,
	 * org.apache.wicket.markup.html.form.Form)
	 */
	@Override
	protected void onSubmit(final AjaxRequestTarget target, final Form<?> form) {
		super.onSubmit(target, form);

		xAxis.getObject().removePlotBand(plotBand.getObject());
		target.add(form);
	}

}
