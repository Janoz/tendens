package nb.tendens.interfaces.charts.configurator.series;

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.EnumChoiceRenderer;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import com.google.common.collect.Lists;

import nb.tendens.application.charts.YAxis;
import nb.tendens.application.charts.series.AbstractSerie;
import nb.tendens.application.charts.series.StepType;

/**
 * CommonSerieConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class CommonSerieConfigurator extends Panel {

	/**
	 * Model.
	 */
	private final IModel<AbstractSerie> serie;

	/**
	 * YAxis.
	 */
	private final IModel<YAxis> yAxis;

	/**
	 * Constructor.
	 *
	 * @param markupId
	 *            markupid.
	 * @param serie
	 *            serie.
	 */
	public CommonSerieConfigurator(final String id, final IModel<YAxis> yAxis, final IModel<AbstractSerie> serie) {
		super(id, serie);

		this.serie = serie;
		this.yAxis = yAxis;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();

		add(new TextField<String>("name", new PropertyModel<String>(serie, "name")));
		add(new TextField<String>("color", new PropertyModel<String>(serie, "color")));
		add(new CheckBox("visible", new PropertyModel<Boolean>(serie, "visible")));
		add(new CheckBox("connectNulls", new PropertyModel<Boolean>(serie, "connectNulls")));
		add(new TextField<String>("valueSuffix", new PropertyModel<String>(serie, "valueSuffix")) {
			/*
			 * (non-Javadoc)
			 *
			 * @see
			 * org.apache.wicket.markup.html.form.FormComponent#shouldTrimInput(
			 * )
			 */
			@Override
			protected boolean shouldTrimInput() {
				return false;
			}
		});
		add(new TextField<Integer>("zIndex", new PropertyModel<Integer>(serie, "zIndex")));
		final DropDownChoice<StepType> stepType = new DropDownChoice<>("stepType",
				new PropertyModel<StepType>(getDefaultModelObject(), "stepType"), Lists.newArrayList(StepType.values()),
				new EnumChoiceRenderer<StepType>(this));
		stepType.setNullValid(true);
		add(stepType);
	}
}
