package nb.tendens.interfaces.charts.configurator.series;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;

import nb.tendens.application.charts.YAxis;
import nb.tendens.application.charts.datasources.DataSource;
import nb.tendens.application.charts.series.AbstractSerie;
import nb.tendens.interfaces.charts.configurator.datasources.DataSourceConfigurator;

/**
 * AreaRangeSerieConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class AreaRangeSerieConfigurator extends AbstractSerieConfigurator {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * YAxisConfigurator.
	 *
	 * @param markupId
	 *            markupId.
	 * @param model
	 *            the model.
	 */
	public AreaRangeSerieConfigurator(final String markupId, final IModel<YAxis> yAxis,
			final IModel<AbstractSerie> serie) {
		super(markupId, yAxis, serie);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		add(new DataSourceConfigurator("dataSourceHigh", new PropertyModel<DataSource>(serie, "dataSourceHigh")) {
			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected IModel<String> getMeterHeadingModel() {
				return new ResourceModel("label.high");
			}
		});
		add(new DataSourceConfigurator("dataSourceLow", new PropertyModel<DataSource>(serie, "dataSourceLow")) {
			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected IModel<String> getMeterHeadingModel() {
				return new ResourceModel("label.low");
			}
		});

	}
}
