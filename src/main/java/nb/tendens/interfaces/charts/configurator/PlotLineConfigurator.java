package nb.tendens.interfaces.charts.configurator;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.EnumChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import com.google.common.collect.Lists;

import nb.tendens.application.charts.DashStyle;
import nb.tendens.application.charts.PlotLine;
import nb.tendens.application.charts.YAxis;

/**
 * PlotLineConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class PlotLineConfigurator extends Panel {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ChartConfigurationModel.
	 */
	private final IModel<PlotLine> plotLine;

	/**
	 * YAxis.
	 */
	private final IModel<YAxis> yAxis;

	/**
	 * YAxisConfigurator.
	 *
	 * @param markupId
	 *            markupId.
	 * @param yAxis
	 *            the yAxis model.
	 * @param plotLine
	 *            the plotLine model.
	 */
	public PlotLineConfigurator(final String markupId, final IModel<YAxis> yAxis, final IModel<PlotLine> plotLine) {
		super(markupId, plotLine);

		this.yAxis = yAxis;
		this.plotLine = plotLine;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		add(new DeletePlotLineLink("deletePlotLine", yAxis, plotLine));

		add(new TextField<String>("label", new PropertyModel<String>(plotLine, "label")));
		add(new TextField<String>("color", new PropertyModel<String>(plotLine, "color")));
		add(new TextField<String>("value", new PropertyModel<String>(plotLine, "value")));
		final DropDownChoice<DashStyle> dashStyle = new DropDownChoice<>("dashStyle",
				new PropertyModel<DashStyle>(getDefaultModelObject(), "dashStyle"),
				Lists.newArrayList(DashStyle.values()), new EnumChoiceRenderer<DashStyle>(this));
		dashStyle.setNullValid(false);
		add(dashStyle);
		add(new TextField<Integer>("zIndex", new PropertyModel<Integer>(plotLine, "zIndex")));

	}
}
