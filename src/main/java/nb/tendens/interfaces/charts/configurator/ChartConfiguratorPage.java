package nb.tendens.interfaces.charts.configurator;

import java.util.Date;
import java.util.List;

import org.apache.wicket.datetime.markup.html.basic.DateLabel;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.charts.XAxis;
import nb.tendens.application.charts.YAxis;
import nb.tendens.interfaces.charts.ChartView;
import nb.tendens.interfaces.components.StylishFeedbackPanel;
import nb.tendens.interfaces.components.models.IntervalModel;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import nb.tendens.interfaces.home.BasePage;

/**
 * ChartConfiguratorPage.
 *
 * @author N. Bulthuis
 *
 */
public class ChartConfiguratorPage extends BasePage {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ChartConfiguration Model.
	 */
	private final IModel<ChartConfiguration> chartConfigurationModel;

	/**
	 * Create a new ChartConfiguratorPage with the specified model.
	 *
	 * @param model
	 *            the model.
	 */
	public ChartConfiguratorPage(final IModel<ChartConfiguration> model) {
		chartConfigurationModel = model;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.interfaces.home.BasePage#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		final Form<ChartConfiguration> form = new Form<ChartConfiguration>("form");
		form.setOutputMarkupId(true);
		form.add(new StylishFeedbackPanel("feedback"));

		final SaveChartLink saveChart = new SaveChartLink("saveChart", chartConfigurationModel);
		form.add(saveChart);
		final TextField<String> nameField = new TextField<String>("name",
				new PropertyModel<String>(chartConfigurationModel, "name")) {
			@Override
			public boolean isRequired() {
				final Form form = findParent(Form.class);
				return form.getRootForm().findSubmittingButton() == saveChart;
			}
		};
		form.add(nameField);

		form.add(new TextField<>("from", new PropertyModel<String>(chartConfigurationModel, "from")));
		form.add(new TextField<>("until", new PropertyModel<String>(chartConfigurationModel, "until")));

		form.add(DateLabel.forDatePattern("intervalFrom",
				new IntervalModel(new PropertyModel<String>(chartConfigurationModel, "from"), Model.of(new Date())),
				"yyyy-MM-dd HH:mm:ss"));
		form.add(DateLabel.forDatePattern("intervalUntil",
				new IntervalModel(new PropertyModel<String>(chartConfigurationModel, "until"), Model.of(new Date())),
				"yyyy-MM-dd HH:mm:ss"));

		form.add(new ChartView("chartView", chartConfigurationModel));

		form.add(new AddYAxisLink("addYAxis", chartConfigurationModel));

		final ListView<YAxis> yAxes = new ListView<YAxis>("yAxes",
				new PropertyModel<List<YAxis>>(chartConfigurationModel, "yAxes")) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final ListItem<YAxis> item) {
				item.add(new YAxisConfigurator("yAxis", chartConfigurationModel, item.getModel()));

			}
		};
		form.add(yAxes);

		form.add(new RedrawChartLink("redrawChart"));
		form.add(new TextField<>("height", new PropertyModel<String>(chartConfigurationModel, "height")));
		form.add(new XAxisConfigurator("xAxis", chartConfigurationModel,
				new PropertyModel<XAxis>(chartConfigurationModel, "xAxis")));
		add(form);

	}
}
