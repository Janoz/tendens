package nb.tendens.interfaces.charts.configurator;

import java.util.List;

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.charts.PlotLine;
import nb.tendens.application.charts.YAxis;
import nb.tendens.application.charts.series.AbstractSerie;
import nb.tendens.application.charts.series.AreaRangeSerie;
import nb.tendens.application.charts.series.BasicSerie;
import nb.tendens.application.charts.series.SerieType;
import nb.tendens.interfaces.charts.configurator.series.AddSerieLink;
import nb.tendens.interfaces.charts.configurator.series.AreaRangeSerieConfigurator;
import nb.tendens.interfaces.charts.configurator.series.BasicSerieConfigurator;

/**
 * YAxisConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class YAxisConfigurator extends Panel {

	/**
	 * AddAreaRangeSerieLink.
	 *
	 * @author N. Bulthuis
	 *
	 */
	private class AddAreaRangeSerieLink extends AddSerieLink {

		/**
		 * Serial ID.
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Serie Type.
		 */
		private final SerieType serieType;

		/**
		 * AddDefaultSerieLink.
		 *
		 * @param markupId
		 * @param model
		 */
		public AddAreaRangeSerieLink(final String markupId, final IModel<YAxis> model, final SerieType serieType) {
			super(markupId, model);
			this.serieType = serieType;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public AbstractSerie newSerie() {
			return new AreaRangeSerie(serieType);
		}

	}

	/**
	 * AddBasicSerieLink.
	 *
	 * @author N. Bulthuis
	 *
	 */
	private class AddBasicSerieLink extends AddSerieLink {

		/**
		 * Serial ID.
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Serie Type.
		 */
		private final SerieType serieType;

		/**
		 * AddDefaultSerieLink.
		 *
		 * @param markupId
		 * @param model
		 */
		public AddBasicSerieLink(final String markupId, final IModel<YAxis> model, final SerieType serieType) {
			super(markupId, model);
			this.serieType = serieType;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public AbstractSerie newSerie() {
			return new BasicSerie(serieType);
		}

	}

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ChartConfigurationModel.
	 */
	private final IModel<ChartConfiguration> model;

	/**
	 * YAxis.
	 */
	private final IModel<YAxis> yAxis;

	/**
	 * YAxisConfigurator.
	 *
	 * @param markupId
	 *            markupId.
	 * @param model
	 *            the model.
	 */
	public YAxisConfigurator(final String markupId, final IModel<ChartConfiguration> model, final IModel<YAxis> yAxis) {
		super(markupId, yAxis);

		this.yAxis = yAxis;
		this.model = model;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();
		add(new AddBasicSerieLink("addSerieLine", yAxis, SerieType.LINE));
		add(new AddBasicSerieLink("addSerieSpline", yAxis, SerieType.SPLINE));
		add(new AddBasicSerieLink("addSerieArea", yAxis, SerieType.AREA));
		add(new AddBasicSerieLink("addSerieAreaSpline", yAxis, SerieType.AREASPLINE));
		add(new AddAreaRangeSerieLink("addSerieAreaRange", yAxis, SerieType.AREARANGE));
		add(new AddAreaRangeSerieLink("addSerieAreaSplineRange", yAxis, SerieType.AREASPLINERANGE));
		add(new AddBasicSerieLink("addSerieColumn", yAxis, SerieType.COLUMN));
		add(new AddPlotLineLink("addPlotLine", yAxis));
		add(new DeleteYAxisLink("deleteYAxis", model, yAxis));
		add(new TextField<String>("name", new PropertyModel<String>(yAxis, "name")));
		add(new TextField<String>("labelFormat", new PropertyModel<String>(yAxis, "labelFormat")));
		add(new TextField<String>("top", new PropertyModel<String>(yAxis, "top")));
		add(new TextField<String>("height", new PropertyModel<String>(yAxis, "height")));
		add(new TextField<Integer>("offset", new PropertyModel<Integer>(yAxis, "offset")));
		add(new TextField<Double>("min", new PropertyModel<Double>(yAxis, "min")));
		add(new TextField<Double>("max", new PropertyModel<Double>(yAxis, "max")));
		add(new CheckBox("opposite", new PropertyModel<Boolean>(yAxis, "opposite")));
		add(new CheckBox("startOnTick", new PropertyModel<Boolean>(yAxis, "startOnTick")));
		add(new CheckBox("endOnTick", new PropertyModel<Boolean>(yAxis, "endOnTick")));
		add(new TextField<Integer>("tickAmount", new PropertyModel<Integer>(yAxis, "tickAmount")));
		add(new TextField<Double>("floor", new PropertyModel<Double>(yAxis, "floor")));
		add(new TextField<Double>("ceiling", new PropertyModel<Double>(yAxis, "ceiling")));

		final ListView<AbstractSerie> series = new ListView<AbstractSerie>("series",
				new PropertyModel<List<AbstractSerie>>(yAxis, "series")) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final ListItem<AbstractSerie> item) {

				if (item.getModelObject() instanceof AreaRangeSerie) {
					item.add(new AreaRangeSerieConfigurator("serie", yAxis, item.getModel()));
				} else {
					item.add(new BasicSerieConfigurator("serie", yAxis, item.getModel()));
				}
			}
		};
		add(series);
		// series.setReuseItems(true);

		final ListView<PlotLine> plotLines = new ListView<PlotLine>("plotLines",
				new PropertyModel<List<PlotLine>>(yAxis, "plotLines")) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final ListItem<PlotLine> item) {

				item.add(new PlotLineConfigurator("plotLine", yAxis, item.getModel()));

			}
		};
		add(plotLines);
		// plotLines.setReuseItems(true);
	}
}
