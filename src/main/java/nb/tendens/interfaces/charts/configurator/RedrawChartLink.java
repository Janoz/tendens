package nb.tendens.interfaces.charts.configurator;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;

import nb.tendens.interfaces.components.DefaultAjaxSubmitLink;

/**
 * RedrawChartLink.
 *
 * @author N. Bulthuis
 *
 */
public class RedrawChartLink extends DefaultAjaxSubmitLink {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 *
	 * @param markupId
	 *            the markup.
	 */
	public RedrawChartLink(final String markupId) {
		super(markupId);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink#onError(org.apache
	 * .wicket.ajax.AjaxRequestTarget, org.apache.wicket.markup.html.form.Form)
	 */
	@Override
	protected void onError(final AjaxRequestTarget target, final Form<?> form) {
		super.onError(target, form);
		target.add(form);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink#onSubmit(org.
	 * apache.wicket.ajax.AjaxRequestTarget,
	 * org.apache.wicket.markup.html.form.Form)
	 */
	@Override
	protected void onSubmit(final AjaxRequestTarget target, final Form<?> form) {
		super.onSubmit(target, form);

		target.add(form);

	}

}
