package nb.tendens.interfaces.charts.configurator;

import java.util.List;

import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.charts.PlotBand;
import nb.tendens.application.charts.XAxis;

/**
 * YAxisConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class XAxisConfigurator extends Panel {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ChartConfigurationModel.
	 */
	private final IModel<ChartConfiguration> model;

	/**
	 * YAxis.
	 */
	private final IModel<XAxis> xAxis;

	/**
	 * XAxisConfigurator.
	 *
	 * @param markupId
	 *            markupId.
	 * @param model
	 *            the model.
	 */
	public XAxisConfigurator(final String markupId, final IModel<ChartConfiguration> model, final IModel<XAxis> xAxis) {
		super(markupId, xAxis);

		this.xAxis = xAxis;
		this.model = model;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();
		add(new AddPlotBandLink("addPlotBand", xAxis));

		final ListView<PlotBand> plotBands = new ListView<PlotBand>("plotBands",
				new PropertyModel<List<PlotBand>>(xAxis, "plotBands")) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final ListItem<PlotBand> item) {

				item.add(new PlotBandConfigurator("plotBand", xAxis, item.getModel()));

			}
		};
		add(plotBands);
		// plotLines.setReuseItems(true);
	}
}
