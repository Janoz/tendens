package nb.tendens.interfaces.charts.configurator.datasources;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.EnumChoiceRenderer;
import org.apache.wicket.markup.html.panel.Panel;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;

import com.google.common.collect.Lists;

import nb.tendens.application.charts.datasources.Aggregator;
import nb.tendens.application.charts.datasources.DataSource;
import nb.tendens.application.charts.datasources.Reductor;
import nb.tendens.application.meters.Meter;
import nb.tendens.interfaces.components.choicerenderers.MeterChoiceRenderer;
import nb.tendens.interfaces.components.models.MeterListLDM;

/**
 * DataSourceConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class DataSourceConfigurator extends Panel {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * DataSourceConfigurator.
	 *
	 * @param markupId
	 *            markupId.
	 * @param dataSource
	 *            the dataSource model.
	 */
	public DataSourceConfigurator(final String markupId, final IModel<DataSource> dataSource) {
		super(markupId, dataSource);

	}

	/**
	 * Get the meter heading model.
	 *
	 * @return the model.
	 */
	protected IModel<String> getMeterHeadingModel() {
		return new ResourceModel("label.meter");
	}

	/**
	 * New Heading Meter label.
	 *
	 * @param markupId
	 *            the markup id.
	 * @return the label.
	 */
	private Label newMeterHeadingLabel(final String markupId) {
		return new Label(markupId, getMeterHeadingModel());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		add(newMeterHeadingLabel("meterHeading"));

		final DropDownChoice<Meter> meter = new DropDownChoice<>("meter",
				new PropertyModel<Meter>(getDefaultModelObject(), "meter"), new MeterListLDM(),
				new MeterChoiceRenderer());
		meter.setNullValid(false);
		add(meter);

		final DropDownChoice<Reductor> reductor = new DropDownChoice<>("reductor",
				new PropertyModel<Reductor>(getDefaultModelObject(), "reductor"), Lists.newArrayList(Reductor.values()),
				new EnumChoiceRenderer<Reductor>(this));
		reductor.setNullValid(false);
		add(reductor);

		final DropDownChoice<Aggregator> aggregator = new DropDownChoice<>("aggregator",
				new PropertyModel<Aggregator>(getDefaultModelObject(), "aggregator"),
				Lists.newArrayList(Aggregator.values()), new EnumChoiceRenderer<Aggregator>(this));
		reductor.setNullValid(false);
		add(aggregator);

		add(new CheckBox("addMissingIntermediateValues",
				new PropertyModel<Boolean>(getDefaultModelObject(), "addMissingIntermediateValues")));
		add(new CheckBox("addMissingEndValues",
				new PropertyModel<Boolean>(getDefaultModelObject(), "addMissingEndValues")));
	}
}
