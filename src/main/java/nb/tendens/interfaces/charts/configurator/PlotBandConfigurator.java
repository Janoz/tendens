package nb.tendens.interfaces.charts.configurator;

import org.apache.wicket.markup.html.form.TextField;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import nb.tendens.application.charts.PlotBand;
import nb.tendens.application.charts.XAxis;
import nb.tendens.application.charts.datasources.DataSource;
import nb.tendens.interfaces.charts.configurator.datasources.DataSourceConfigurator;

/**
 * PlotLineConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class PlotBandConfigurator extends Panel {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ChartConfigurationModel.
	 */
	private final IModel<PlotBand> plotBand;

	/**
	 * xAxis.
	 */
	private final IModel<XAxis> xAxis;

	/**
	 * YAxisConfigurator.
	 *
	 * @param markupId
	 *            markupId.
	 * @param yAxis
	 *            the yAxis model.
	 * @param plotBand
	 *            the plotLine model.
	 */
	public PlotBandConfigurator(final String markupId, final IModel<XAxis> xAxis, final IModel<PlotBand> plotBand) {
		super(markupId, plotBand);

		this.xAxis = xAxis;
		this.plotBand = plotBand;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		add(new DeletePlotBandLink("deletePlotBand", xAxis, plotBand));
		add(new TextField<String>("label", new PropertyModel<String>(plotBand, "label")));
		add(new TextField<String>("color", new PropertyModel<String>(plotBand, "color")));
		add(new TextField<String>("onValue", new PropertyModel<String>(plotBand, "onValue")));
		add(new TextField<String>("offValue", new PropertyModel<String>(plotBand, "offValue")));
		add(new TextField<Integer>("zIndex", new PropertyModel<Integer>(plotBand, "zIndex")));
		add(new DataSourceConfigurator("dataSource", new PropertyModel<DataSource>(plotBand, "dataSource")));

	}
}
