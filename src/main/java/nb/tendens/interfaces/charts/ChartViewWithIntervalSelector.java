package nb.tendens.interfaces.charts;

import java.util.Date;

import org.apache.wicket.datetime.markup.html.basic.DateLabel;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.interfaces.components.StylishFeedbackPanel;
import nb.tendens.interfaces.components.models.IntervalModel;

/**
 * ChartViewWithIntervalSelector.
 *
 * @author N. Bulthuis
 *
 */
public class ChartViewWithIntervalSelector extends Panel {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Model.
	 */
	private final IModel<ChartConfiguration> model;

	/**
	 * Constructor.
	 *
	 * @param markupId
	 *            markup id.
	 * @param model
	 *            model.
	 */
	public ChartViewWithIntervalSelector(final String markupId, final IModel<ChartConfiguration> model) {
		super(markupId, model);

		setOutputMarkupId(true);

		this.model = model;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();

		final Form<ChartConfiguration> form = new Form<ChartConfiguration>("form");
		form.setOutputMarkupId(true);

		form.add(new ChartView("chartView", model));
		form.add(new StylishFeedbackPanel("feedback"));

		form.add(new TextField<>("from", new PropertyModel<String>(model, "from")));
		form.add(new TextField<>("until", new PropertyModel<String>(model, "until")));

		form.add(DateLabel.forDatePattern("intervalFrom",
				new IntervalModel(new PropertyModel<String>(model, "from"), Model.of(new Date())),
				"yyyy-MM-dd HH:mm:ss"));
		form.add(DateLabel.forDatePattern("intervalUntil",
				new IntervalModel(new PropertyModel<String>(model, "until"), Model.of(new Date())),
				"yyyy-MM-dd HH:mm:ss"));

		add(form);

	}

}
