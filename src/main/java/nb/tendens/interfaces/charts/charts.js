;(function (undefined) {
        "use strict";

        if (typeof (window.tendens) === "undefined") {
          	
    		window.tendens = new Object();
        }
        
        if (typeof String.prototype.startsWith != 'function') {
        	String.prototype.startsWith = function (str) {
        	    return this.slice(0, str.length) == str;
        	};
        }
        
        if (typeof String.prototype.form != 'function') {
          String.prototype.format = function() {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function(match, number) { 
              return typeof args[number] != 'undefined'
                ? args[number]
                : match
              ;
            });
          };
        }
        
        
        if (typeof Number.prototype.round != 'function') {
        
	        Number.prototype.round = function(places) {
	        	  return +(Math.round(this + "e+" + places)  + "e-" + places);
	        };
        }
        
        if (typeof (tendens.toColor) === "undefined") {
        	
        	tendens.toColor = function(colorstr) {
        		
        		if (colorstr.startsWith("@") || colorstr.startsWith("|")) {
        			
        			var gradient = {
        					stops: []
        			};
        			
    				var stops = colorstr.substring(1).split(':');
    				
    				stops.forEach(function(stop) {
    					var e = stop.split(",");
    					gradient.stops.push([ parseFloat(e[0]), 'rgba({0}, {1}, {2}, {3})'.format(e[1], e[2], e[3], e[4]) ] );
    				});
        			
        			if (colorstr.startsWith("@")) {
        				gradient['radialGradient'] = { cx: 0.5, cy: 0.3, r: 0.7 }
        			} else {
        				gradient['linearGradient'] = { x1 : 0, y1 : 0, x2 : 0, y2 : 1 };
        			}

        			return gradient;
        			
        		} else {
        			return colorstr;
        		}
        	}
        }
        
        
        if (typeof (tendens.flexChart) === "undefined") {
        	
         	// try to find an id.. if we do not have one, check our parent tree
    		tendens.flexChart = function(componentId, dataUrl)  {

    			console.log("Chart(Begin) - FlexChart");
       			
    			var options = {
    		            chart: {
    		                zoomType: 'x',
    		            },
    		            title: {
    		                text: ''
    		            },
    		            xAxis: {
    		                type: 'datetime'
    		            },
    		            tooltip: {
    		        		crosshairs : true,
    						shared : true
    					},
    		            credits : {
    						enabled : false
    					},
    		            yAxis: [],
    		            series: []
    		        };
    			
    			$.getJSON(dataUrl, function (data) {

    				var config = data['config'];
    				var metrics = data['data'];
    				
    				// set the title for the chart.
    				// options.title['text'] = config['name'];
					if (config.height !== null) {
						options.chart['height'] = config['height'];
					}	    
					
					
					if (config['xAxis']['plotBands'] !== null) {
						
						config['xAxis']['plotBands'].forEach(function (plotBand, plotBandIndex) {
							console.log(metrics[plotBand['id']]);
							
   							if (metrics[plotBand['id']]) {
    							metrics[plotBand['id']].forEach(function (metric, metricIndex) {
    								console.log(metric);
    							});
   							}							
							
						});
					}
					
    				
    				config['yAxes'].forEach(function (yAxis, yAxisIndex) {
    					
    					var y = {
    						title: {
    							text: yAxis['name']
    						},
    						labels: {
    							format: yAxis['labelFormat']
    						},
    						opposite: yAxis['opposite'],
    						startOnTick: yAxis['startOnTick'],
    						endOnTick: yAxis['endOnTick'],
    						plotLines: []
    					};
    					
						if (yAxis.tickAmount !== null) {
							y['tickAmount'] = yAxis['tickAmount'];
						}

						if (yAxis.floor !== null) {
							y['floor'] = yAxis['floor'];
						}

						if (yAxis.ceiling !== null) {
							y['ceiling'] = yAxis['ceiling'];
						}

						if (yAxis.top !== null) {
							y['top'] = yAxis['top'];
						}
						
						if (yAxis.height !== null) {
							y['height'] = yAxis['height'];
						}
						if (yAxis.offset !== null) {
							y['offset'] = yAxis['offset'];
						}
						
						if (yAxis.min !== null) {
							y['min'] = yAxis['min'];
						}
						if (yAxis.max !== null) {
							y['max'] = yAxis['max'];
						}						
    					yAxis['plotLines'].forEach(function (plotLine, plotLineIndex) {
    					
    						var pl = {
    								value: plotLine['value'],
    								dashStyle: plotLine['dashStyle'],
    								label: {
    									text: plotLine['label']
    								},
    								width: 2,
    								zIndex: 100,
    						};

							if (plotLine.color !== null) {
								pl['color'] = tendens.toColor(plotLine['color']);
							}
							
							if (plotLine.zIndex !== null) {
								pl['zIndex'] = plotLine['zIndex'];
							}							

    						y.plotLines.push(pl);
    						
    					})
    					options.yAxis.push(y);
    					
    					yAxis['series'].forEach(function (serie, serieIndex) {
    						
    							var s = {
    								name: serie['name'],
    								data: [], 
    								yAxis: yAxisIndex,
    								type: serie['serieType'].toLowerCase(),
    								tooltip: {
    									valueSuffix: serie['valueSuffix']
    								}
    							};
    							
    							if (serie.color !== null) {
    								s['color'] = tendens.toColor(serie['color']);
    							}

    							if (serie.zIndex !== null) {
    								s['zIndex'] = serie['zIndex'];
    							}
    							
       							if (serie.visible !== null) {
    								s['visible'] = serie['visible'];
    							} 
       							
       							if (serie.connectNulls !== null) {
    								s['connectNulls'] = serie['connectNulls'];
    							} 
       							if (serie.stepType !== null) {
    								s['step'] = serie['stepType'].toLowerCase();
    							}   
       							
    							if (serie.stackingGroup !== null) {
    								s['stack'] = serie['stackingGroup'];
    							}
    							
    							if (serie.stackingType !== null) {
    								s['stacking'] = serie['stackingType'].toLowerCase();
    							}
    							
       							if (metrics[serie['id']]) {
	    							metrics[serie['id']].forEach(function (metric, metricIndex) {
	
	    								if (serie['serieType'] === 'AREARANGE' || serie['serieType'] === 'AREASPLINERANGE') {
	    									
	    									var high = null;
	    									var low = null;
	    									
	    									if (metric['high'] !== null) {
	    										high = metric['high']['value'].round(2);
	    									}
	    									if (metric['low'] !== null) {
	    										low = metric['low']['value'].round(2);
	    									}
	    									
	    									s.data.push([metric['dateTime'], high, low]);
	    								} else {
	    									s.data.push([metric['dateTime'], metric['value'].round(2)]);
	    								}
	    							});
	    							options.series.push(s);
       							}
    					});

    				
    				});
    				
   
    		        $('#'+componentId).highcharts(options);
    		    });
  
    			
	 	        console.log("Chart(End) - Flex");
    		};
        }
})();