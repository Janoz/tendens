package nb.tendens.interfaces.charts;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.wicket.behavior.AbstractAjaxBehavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;

/*
 * #%L
 * zdesk
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.TextRequestHandler;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.owlike.genson.Genson;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.charts.PlotBand;
import nb.tendens.application.charts.YAxis;
import nb.tendens.application.charts.series.AbstractSerie;
import nb.tendens.application.metrics.CalculatedMetric;
import nb.tendens.infrastructure.utils.Interval;
import nb.tendens.infrastructure.utils.JsonUtilities;
import nb.tendens.interfaces.WicketApplication;
import nb.tendens.interfaces.resources.javascript.JavaScript;

/**
 * ChartView.
 *
 * @author N. Bulthuis
 *
 */
public class ChartView extends Panel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Charts JS.
	 */
	private static final JavaScriptHeaderItem JS_CHARTS = JavaScriptHeaderItem
			.forReference(new JavaScriptResourceReference(ChartView.class, "charts.js"));

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ChartView.class);

	/**
	 * Chart Container.
	 */
	private final WebMarkupContainer chartContainer = new WebMarkupContainer("chartContainer");

	/**
	 * Chart Data.
	 */
	private AbstractAjaxBehavior chartData;

	/**
	 * ChartConfiguration Model.
	 */
	private final IModel<ChartConfiguration> model;

	/**
	 * Constructor.
	 *
	 * @param markupId
	 *            markup id.
	 * @param model
	 *            model.
	 */
	public ChartView(final String markupId, final IModel<ChartConfiguration> model) {
		super(markupId, model);
		setOutputMarkupId(true);

		this.model = model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		add(chartContainer);

		this.chartData = new AbstractAjaxBehavior() {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onRequest() {

				LOG.info("Collecting ChartData");

				final ChartConfiguration cc = model.getObject();

				final RequestCycle cycle = RequestCycle.get();

				final Genson genson = JsonUtilities.newGenson();

				final Date fromDate = Interval.parse(cc.getFrom(), new Date());
				final Date untilDate = Interval.parse(cc.getUntil(), new Date());

				final Map<String, List<CalculatedMetric>> data = Maps.newConcurrentMap();

				LOG.info("From: {}", fromDate);
				LOG.info("Until: {}", untilDate);

				for (final YAxis yAxis : cc.getYAxes()) {

					for (final AbstractSerie gSerie : yAxis.getSeries()) {

						if (gSerie.isValid()) {
							final List<CalculatedMetric> metrics = WicketApplication.get().getManagerFactory()
									.getMetricManager().findForSerie(gSerie, fromDate, untilDate);
							LOG.debug("Metrics: {} => {}", gSerie.getName(), metrics.size());
							data.put(gSerie.getId(), metrics);
						}
					}
				}

				for (final PlotBand plotBand : cc.getXAxis().getPlotBands()) {

					if (plotBand.isValid()) {

						final List<CalculatedMetric> metrics = Lists.newArrayList();

						metrics.addAll(WicketApplication.get().getManagerFactory().getMetricManager()
								.findForDataSource(plotBand.getDataSource(), fromDate, untilDate));

						LOG.debug("PlotBand.Metrics: {} => {}", metrics.size());
						data.put(plotBand.getId(), metrics);
					}
				}

				// System.out.println(genson.serialize(cc));

				final Map<String, Object> configurationAndData = Maps.newConcurrentMap();
				configurationAndData.put("config", cc);
				configurationAndData.put("data", data);

				final String json = genson.serialize(configurationAndData);
				cycle.scheduleRequestHandlerAfterCurrent(new TextRequestHandler("application/json", "UTF-8", json));
			}
		};
		add(chartData);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void renderHead(final IHeaderResponse response) {
		super.renderHead(response);

		JavaScript.enableHighCharts(response);
		response.render(JS_CHARTS);

		final String script = String.format("tendens.flexChart(\"%s\", \"%s\")", chartContainer.getMarkupId(),
				chartData.getCallbackUrl());
		response.render(OnDomReadyHeaderItem.forScript(script));
	}

}
