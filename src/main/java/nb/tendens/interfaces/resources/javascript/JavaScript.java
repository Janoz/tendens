package nb.tendens.interfaces.resources.javascript;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

/**
 * JavaScript Renderer.
 *
 * @author N. Bulthuis
 *
 */
public final class JavaScript {

	/**
	 * Highcharts JavaScript.
	 */
	private static final JavaScriptHeaderItem HIGHCHARTS = JavaScriptHeaderItem
			.forReference(new JavaScriptResourceReference(JavaScript.class, "highcharts/highcharts.js"));
	/**
	 * Highcharts JavaScript More.
	 */
	private static final JavaScriptHeaderItem HIGHCHARTS_MORE = JavaScriptHeaderItem
			.forReference(new JavaScriptResourceReference(JavaScript.class, "highcharts/highcharts-more.js"));
	/**
	 * Highcharts JavaScript.
	 */
	private static final JavaScriptHeaderItem HIGHCHARTS_OPTIONS = JavaScriptHeaderItem
			.forReference(new JavaScriptResourceReference(JavaScript.class, "highcharts/highcharts-options.js"));

	/**
	 * Enable HighCharts.
	 *
	 * @param response
	 *            headerreponse for rendering javascript items.
	 */
	public static void enableHighCharts(final IHeaderResponse response) {

		enableJQuery(response);
		response.render(HIGHCHARTS);
		response.render(HIGHCHARTS_OPTIONS);
		response.render(HIGHCHARTS_MORE);
	}

	/**
	 * Enable JQuery.
	 *
	 * @param response
	 *            headerreponse for rendering javascript items.
	 */
	public static void enableJQuery(final IHeaderResponse response) {

		response.render(JavaScriptHeaderItem
				.forReference(WebApplication.get().getJavaScriptLibrarySettings().getJQueryReference()));
	}

	/**
	 * Hidden.
	 */
	private JavaScript() {

	}

}
