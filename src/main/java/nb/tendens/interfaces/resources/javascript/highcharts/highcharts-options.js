$(function() {

Highcharts.theme = {  
   global:{  
	      useUTC:false
	   },
	   lang:{  
	      thousandsSep:'.',
	      decimalPoint:','
	   },
	   colors:[  
	      "#2b908f", // licht blauw
	      "#90ee7e", // licht groen
	      "#f45b5b", // licht rood
	      "#7798BF", // licht paars/blauw
	      "#aaeeee", // aqua
	      "#ff0066", // donker roze
	      "#eeaaee", // licht roze
	      "#55BF3B", // groen
	      "rgba(255, 0, 0, 0.8)", // rood
	      "rgba(255, 165, 0, 0.8)", // geel oranje
	      "rgba(0, 0, 255, 0.8)", // blauw
	      
	   ],
	   plotOptions: {
		   line: {
			   marker: {
				   enabled: false,
			   },
		   },
		   spline: {
			   marker: {
				   enabled: false,
			   },
		   },
		   area: {
			   marker: {
				   enabled: false,
			   },
		   },	   
		   areaspline: {
			   marker: {
				   enabled: false,
			   },
		   },	   
	   },
	   chart:{  
		   backgroundColor:'rgba(255, 255, 255, 0.1)',
	      style:{  
	         fontFamily:"'Open Sans', sans-serif"
	      },
	   },
	};

    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);
    // Radialize the colors
    Highcharts.getOptions().gradient_colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });    

  	// First, let's make the colors transparent
  	Highcharts.getOptions().transparant_colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
      return {
        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
        stops: [
            [0, Highcharts.Color(color).setOpacity(0.8).get('rgba')],
            [1, Highcharts.Color(color).brighten(-0.3).setOpacity(0.8).get('rgba')] // darken
        ]
    };
  	});  
    
});