package nb.tendens.interfaces.dashboards.configurator;

import java.util.Date;

import org.apache.wicket.datetime.markup.html.basic.DateLabel;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.TextField;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.dashboards.Dashboard;
import nb.tendens.application.dashboards.DashboardItem;
import nb.tendens.interfaces.WicketApplication;
import nb.tendens.interfaces.charts.ChartView;
import nb.tendens.interfaces.components.choicerenderers.ChartIdChoiceRenderer;
import nb.tendens.interfaces.components.models.ChartIdListLDM;
import nb.tendens.interfaces.components.models.IntervalModel;

/**
 * DashboardItemConfigurator.
 *
 * @author N. Bulthuis
 *
 */
public class DashboardItemConfigurator extends Panel {

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(DashboardItemConfigurator.class);

	/**
	 * Dashboard Model.
	 */
	private final IModel<Dashboard> dashboard;

	/**
	 * DashboardItem Model.
	 */
	private final IModel<DashboardItem> dashboardItem;

	/**
	 * DashboardItemConfigurator.
	 *
	 * @param markupId
	 *            markupId.
	 * @param model
	 *            the model.
	 */
	public DashboardItemConfigurator(final String markupId, final IModel<Dashboard> dashboard,
			final IModel<DashboardItem> dashboardItem) {
		super(markupId, dashboardItem);

		setOutputMarkupId(true);

		this.dashboard = dashboard;
		this.dashboardItem = dashboardItem;
	}

	public ChartConfiguration getChartConfiguration() {

		final Long chartId = dashboardItem.getObject().getChartId();

		if (chartId != null) {

			final ChartConfiguration cc = WicketApplication.get().getManagerFactory().getChartManager()
					.findById(dashboardItem.getObject().getChartId());

			cc.setFrom(dashboardItem.getObject().getFrom());
			cc.setUntil(dashboardItem.getObject().getUntil());
			return cc;
		} else {
			return ChartConfiguration.newDefault();
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		add(new DeleteDashboardItemLink("deleteDashboardItem", dashboard, dashboardItem));
		final DropDownChoice<Long> chartSelector = new DropDownChoice<Long>("chartSelector",
				new PropertyModel<Long>(dashboardItem, "chartId"), new ChartIdListLDM(), new ChartIdChoiceRenderer()) {

			@Override
			protected void onSelectionChanged(final Long newSelection) {

				final ChartConfiguration cc = WicketApplication.get().getManagerFactory().getChartManager()
						.findById(newSelection);

				if (cc != null) {

					dashboardItem.getObject().setFrom(cc.getFrom());
					dashboardItem.getObject().setUntil(cc.getUntil());
				}

			};

			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
			};
		};

		chartSelector.setNullValid(false);
		add(chartSelector);

		add(new ChartView("chartView", new PropertyModel<ChartConfiguration>(this, "chartConfiguration")));

		add(new TextField<>("from", new PropertyModel<String>(dashboardItem, "from")));
		add(new TextField<>("until", new PropertyModel<String>(dashboardItem, "until")));

		add(DateLabel.forDatePattern("intervalFrom",
				new IntervalModel(new PropertyModel<String>(dashboardItem, "from"), Model.of(new Date())),
				"yyyy-MM-dd HH:mm:ss"));
		add(DateLabel.forDatePattern("intervalUntil",
				new IntervalModel(new PropertyModel<String>(dashboardItem, "until"), Model.of(new Date())),
				"yyyy-MM-dd HH:mm:ss"));

	}

}
