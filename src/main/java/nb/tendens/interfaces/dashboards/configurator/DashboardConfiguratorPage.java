package nb.tendens.interfaces.dashboards.configurator;

import java.util.List;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import nb.tendens.application.dashboards.Dashboard;
import nb.tendens.application.dashboards.DashboardItem;
import nb.tendens.interfaces.components.StylishFeedbackPanel;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import nb.tendens.interfaces.home.BasePage;

/**
 * DashboardConfiguratorPage.
 *
 * @author N. Bulthuis
 *
 */
public class DashboardConfiguratorPage extends BasePage {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Dashboard Model.
	 */
	private final IModel<Dashboard> model;

	/**
	 * Create a new DashboardConfiguratorPage with the specified model.
	 *
	 * @param model
	 *            the model.
	 */
	public DashboardConfiguratorPage(final IModel<Dashboard> model) {
		this.model = model;

		setOutputMarkupId(true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.interfaces.home.BasePage#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		final Form<Dashboard> form = new Form<Dashboard>("form");
		form.setOutputMarkupId(true);
		form.add(new StylishFeedbackPanel("feedback"));
		form.add(new RedrawDashboardLink("redrawDashboard"));

		final SaveDashboardLink saveDashboard = new SaveDashboardLink("saveDashboard", model);
		form.add(saveDashboard);
		final TextField<String> nameField = new TextField<String>("name", new PropertyModel<String>(model, "name")) {
			@Override
			public boolean isRequired() {
				final Form form = findParent(Form.class);
				return form.getRootForm().findSubmittingButton() == saveDashboard;
			}
		};
		form.add(nameField);

		form.add(new AddDashboardItemLink("addDashboardItem", model));

		final ListView<DashboardItem> items = new ListView<DashboardItem>("items",
				new PropertyModel<List<DashboardItem>>(model, "items")) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final ListItem<DashboardItem> item) {
				item.add(new DashboardItemConfigurator("item", model, item.getModel()));
			}
		};
		form.add(items);
		add(form);
	}
}
