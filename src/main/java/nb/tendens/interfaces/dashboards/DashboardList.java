package nb.tendens.interfaces.dashboards;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;

import nb.tendens.application.dashboards.Dashboard;
import nb.tendens.interfaces.components.LabelLink;
import nb.tendens.interfaces.components.dataproviders.DashboardDataProvider;

/**
 * DashboardList.
 *
 * @author N. Bulthuis
 *
 */
public class DashboardList extends Panel {

	/**
	 * ActionPanel.
	 *
	 * @author N. Bulthuis
	 *
	 */
	private class ActionPanel extends Panel {

		/**
		 * Serial ID.
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Constructor.
		 *
		 * @param markupId
		 *            markupid.
		 * @param model
		 *            the model.
		 */
		public ActionPanel(final String markupId, final IModel<Dashboard> model) {
			super(markupId, model);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected final void onInitialize() {
			super.onInitialize();

			add(new ConfigureDashboardLink("edit", (IModel<Dashboard>) getDefaultModel()));
			add(new CopyDashboardLink("copy", (IModel<Dashboard>) getDefaultModel()));
			add(new DeleteDashboardLink("delete", (IModel<Dashboard>) getDefaultModel()));
		}
	}

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Filter.
	 */
	private final IModel<String> filter;

	/**
	 * Constructor.
	 *
	 * @param markupId
	 *            markupId.
	 */
	public DashboardList(final String markupId) {
		this(markupId, Model.of(""));
	}

	/**
	 * Constructor.
	 *
	 * @param markupId
	 *            markupId.
	 */
	public DashboardList(final String markupId, final IModel<String> filter) {
		super(markupId);
		this.filter = filter;
	}

	/**
	 * Create a new LabelLink.
	 *
	 * @param componentId
	 *            component id.
	 * @param model
	 *            the model.
	 * @param property
	 *            the property for the label.
	 * @return the label link.
	 */
	private LabelLink<Dashboard> newLabelLink(final String componentId, final IModel<Dashboard> model,
			final String property) {
		final LabelLink<Dashboard> link = new LabelLink<Dashboard>(componentId,
				new PropertyModel<String>(model, property)) {
			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			/*
			 * (non-Javadoc)
			 *
			 * @see nb.tendens.interfaces.components.LabelLink#onClick()
			 */
			@Override
			public void onClick() {
				setResponsePage(new DashboardPage(model));

			}
		};

		return link;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		final DashboardDataProvider dataProvider = new DashboardDataProvider(filter);

		final List<IColumn<Dashboard, String>> columns = new ArrayList<>();
		columns.add(new AbstractColumn<Dashboard, String>(new ResourceModel("label.name"), "name") {
			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void populateItem(final Item<ICellPopulator<Dashboard>> cellItem, final String componentId,
					final IModel<Dashboard> model) {

				cellItem.add(newLabelLink(componentId, model, "name"));
			}
		});
		columns.add(new AbstractColumn<Dashboard, String>(new ResourceModel("label.actions")) {
			/**
			 *
			 */
			private static final long serialVersionUID = 1L;

			/*
			 * (non-Javadoc)
			 *
			 * @see
			 * org.apache.wicket.extensions.markup.html.repeater.data.table.
			 * AbstractColumn#getCssClass()
			 */
			@Override
			public String getCssClass() {
				return "text-right";
			}

			@Override
			public void populateItem(final Item<ICellPopulator<Dashboard>> cellItem, final String componentId,
					final IModel<Dashboard> model) {
				cellItem.add(new ActionPanel(componentId, model));
			}
		});

		final DefaultDataTable<Dashboard, String> dashboardList = new DefaultDataTable<Dashboard, String>(
				"dashboardList", columns, dataProvider, 25);
		add(dashboardList);

	}

}
