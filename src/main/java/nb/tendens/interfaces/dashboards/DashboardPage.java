package nb.tendens.interfaces.dashboards;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import com.google.common.collect.Lists;

import nb.tendens.application.charts.ChartConfiguration;
import nb.tendens.application.dashboards.Dashboard;
import nb.tendens.application.dashboards.DashboardItem;
import nb.tendens.interfaces.WicketApplication;
import nb.tendens.interfaces.charts.ChartViewWithIntervalSelector;
import nb.tendens.interfaces.home.BasePage;

/**
 * DashboardPage.
 *
 * @author N. Bulthuis
 *
 * @since 1.0
 *
 */
public class DashboardPage extends BasePage {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Dashboard.
	 */
	private final IModel<Dashboard> dashboard;

	private final List<ChartConfiguration> charts = Lists.newArrayList();

	/**
	 * Constructor.
	 */
	public DashboardPage(final IModel<Dashboard> dashboard) {
		this.dashboard = dashboard;

		final List<DashboardItem> dashboardItems = dashboard.getObject().getItems();
		for (final DashboardItem dashboardItem : dashboardItems) {
			final ChartConfiguration cc = WicketApplication.get().getManagerFactory().getChartManager()
					.findById(dashboardItem.getChartId());
			cc.setFrom(dashboardItem.getFrom());
			cc.setUntil(dashboardItem.getUntil());

			charts.add(cc);
		}
	}

	public List<ChartConfiguration> getCharts() {
		return charts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		add(new Label("dashboardName", new PropertyModel<String>(dashboard, "name")));

		final ListView<ChartConfiguration> items = new ListView<ChartConfiguration>("items",
				new PropertyModel<List<ChartConfiguration>>(DashboardPage.this, "charts")) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final ListItem<ChartConfiguration> item) {

				final ChartViewWithIntervalSelector chart = new ChartViewWithIntervalSelector("chartView",
						item.getModel());
				item.add(chart);

				item.add(new Label("chartName", new PropertyModel<>(item.getModel(), "name")));

			}
		};
		add(items);

	}
}
