package nb.tendens.interfaces.api.validators;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.wicket.validation.IErrorMessageSource;
import org.apache.wicket.validation.IValidationError;

/**
 * APIValidationError.
 *
 * @author N. Bulthuis
 *
 */
public class APIValidationError implements IValidationError {

	/**
	     *
	     */
	private static final long serialVersionUID = 1L;

	/**
	 * list of message keys to try against the <code>IErrorMessageSource</code>.
	 */
	private final List<String> keys;

	/** variables map to use in variable substitution. */
	private final Map<String, Object> vars;

	/** the field that failed the validation. */
	private final String field;

	/**
	 * Message.
	 */
	private final String errorMessage;

	/**
	 * Instantiates a new rest validation error.
	 *
	 * @param keys
	 *            the keys
	 * @param vars
	 *            the vars
	 * @param field
	 *            the field
	 */
	public APIValidationError(final Class<?> component, final List<String> keys, final Map<String, Object> vars,
			final String field) {
		this.keys = keys;
		this.vars = vars;
		this.field = field;

		final DefaultBundleResolver bundleResolver = new DefaultBundleResolver(component);
		this.errorMessage = getErrorMessage(bundleResolver).toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.apache.wicket.validation.IValidationError#getErrorMessage(org.apache.
	 * wicket.validation.IErrorMessageSource)
	 */
	@Override
	public Serializable getErrorMessage(final IErrorMessageSource messageSource) {
		String errorMessage = null;

		if (keys != null) {
			// try any message keys ...
			for (final String key : keys) {
				errorMessage = messageSource.getMessage(key, vars);
				if (errorMessage != null) {
					break;
				}
			}
		}

		return new APIErrorMessage(this, errorMessage, field);
	}

	/**
	 * Gets the field.
	 *
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * Gets the keys.
	 *
	 * @return the keys
	 */
	public List<String> getKeys() {
		return keys;
	}

	/**
	 * Gets the vars.
	 *
	 * @return the vars
	 */
	public Map<String, Object> getVars() {
		return vars;
	}

	/**
	 * Gets the errormessage.
	 *
	 * @return the errormessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
}
