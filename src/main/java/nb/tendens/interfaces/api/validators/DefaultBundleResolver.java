package nb.tendens.interfaces.api.validators;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * DefaultBundleResolver.
 *
 * @author N. Bulthuis
 *
 */

import org.apache.wicket.Application;
import org.apache.wicket.Session;
import org.apache.wicket.resource.loader.IStringResourceLoader;
import org.apache.wicket.validation.IErrorMessageSource;

/**
 * Wicket bundle resolver that relies on the default
 * {@link IStringResourceLoader}S. Its constructor requires an subclass of
 * {@link AbstractRestResource} which is used to resolve custom bundles.
 *
 * @author andrea del bene
 *
 */
public class DefaultBundleResolver implements IErrorMessageSource {
	private final List<Class<?>> targetClasses;

	public DefaultBundleResolver(final Class<?>... targetClasses) {
		this.targetClasses = Collections.unmodifiableList(Arrays.asList(targetClasses));
	}

	public DefaultBundleResolver(final List<Class<?>> targetClasses) {
		this.targetClasses = Collections.unmodifiableList(targetClasses);
	}

	@Override
	public String getMessage(final String key, final Map<String, Object> vars) {
		String resourceValue = null;
		final List<IStringResourceLoader> resourceLoaders = Application.get().getResourceSettings()
				.getStringResourceLoaders();
		final Locale locale = Session.get().getLocale();
		final String style = Session.get().getStyle();

		outerloop: for (final IStringResourceLoader stringResourceLoader : resourceLoaders) {
			for (final Class<?> clazz : targetClasses) {
				resourceValue = stringResourceLoader.loadStringResource(clazz, key, locale, style, null);

				if (resourceValue != null) {
					break outerloop;
				}
			}
		}

		final StringConverterInterpolator interpolator = new StringConverterInterpolator(
				resourceValue != null ? resourceValue : "", vars, false, locale);

		return interpolator.toString();
	}
}
