package nb.tendens.interfaces.api.validators;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Locale;
import java.util.Map;

import org.apache.wicket.Application;
import org.apache.wicket.util.convert.IConverter;
import org.apache.wicket.util.string.Strings;
import org.apache.wicket.util.string.interpolator.MapVariableInterpolator;

/**
 * StringConverterInterpolator.
 *
 * @author N. Bulthuis
 *
 */
public class StringConverterInterpolator extends MapVariableInterpolator {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private final Locale locale;

	public StringConverterInterpolator(final String string, final Map<?, ?> variables,
			final boolean exceptionOnNullVarValue, final Locale locale) {
		super(string, variables, exceptionOnNullVarValue);
		this.locale = locale;
	}

	private IConverter<?> getConverter(final Class<? extends Object> clazz) {
		return Application.get().getConverterLocator().getConverter(clazz);
	}

	public Locale getLocale() {
		return locale;
	}

	@Override
	protected String getValue(final String variableName) {
		final Object value = super.getValue(variableName);

		if (value == null) {
			return null;
		} else if (value instanceof String) {
			// small optimization - no need to bother with conversion
			// for String vars, e.g. {label}
			return (String) value;
		} else {
			final IConverter converter = getConverter(value.getClass());
			if (converter == null) {
				return Strings.toString(value);
			} else {
				return converter.convertToString(value, getLocale());
			}
		}
	}

}
