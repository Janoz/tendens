package nb.tendens.interfaces.api.validators;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Arrays;
import java.util.Map;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.StringValue;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;

import com.google.common.collect.Maps;

/**
 * MandatoryFieldsValidator.
 *
 * @author N. Bulthuis
 *
 */
public class MandatoryFieldValidator implements IValidator<PageParameters> {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Validation Error Key.
	 */
	private static final String VE_NULL = "validation.null_or_empty";

	/**
	 * Fieldnames.
	 */
	private final String[] fieldNames;

	/**
	 * Create a new Validator.
	 *
	 * @param fieldNames
	 *            the fieldnames.
	 */
	public MandatoryFieldValidator(final String... fieldNames) {
		this.fieldNames = fieldNames;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.validation.IValidator#validate(org.apache.wicket.
	 * validation.IValidatable)
	 */
	@Override
	public final void validate(final IValidatable<PageParameters> validatable) {

		final PageParameters pp = validatable.getValue();

		for (final String param : fieldNames) {
			final StringValue paramValue = pp.get(param);
			if (paramValue.isEmpty()) {
				final Map<String, Object> vars = Maps.newConcurrentMap();
				vars.put("name", param);
				validatable.error(new APIValidationError(this.getClass(), Arrays.asList(VE_NULL), vars, param));
			}

		}

	}

}
