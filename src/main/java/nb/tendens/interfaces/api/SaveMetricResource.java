package nb.tendens.interfaces.api;
/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.ResourceReference;
import org.apache.wicket.util.string.StringValue;
import org.apache.wicket.util.string.StringValueConversionException;
import org.apache.wicket.validation.IValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import nb.tendens.application.meters.Meter;
import nb.tendens.infrastructure.utils.JsonUtilities;
import nb.tendens.interfaces.WicketApplication;
import nb.tendens.interfaces.api.validators.APIValidationError;
import nb.tendens.interfaces.api.validators.MandatoryFieldValidator;

/**
 * SaveMetric.
 *
 * @author N. Bulthuis
 *
 */
public class SaveMetricResource extends AbstractAPIResource {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SaveMetricResource.class);

	/**
	 * Param id.
	 */
	public static final String PARAM_ID = "id";

	/**
	 * Param datetime.
	 */
	public static final String PARAM_DATETIME = "dt";

	/**
	 * Param value.
	 */
	public static final String PARAM_VALUE = "v";

	/**
	 * Create a new ResourceReference to this Resource.
	 *
	 * @return the resource reference to this resources.
	 */
	public static ResourceReference newResourceReference() {
		return new ResourceReference(SaveMetricResource.class.getSimpleName()) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public IResource getResource() {
				return new SaveMetricResource();
			}
		};
	}

	/**
	 * Constructor.
	 */
	public SaveMetricResource() {
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public final List<IValidator<PageParameters>> getValidators() {

		final List<IValidator<PageParameters>> validators = Lists.newArrayList();
		validators.add(new MandatoryFieldValidator(SaveMetricResource.PARAM_ID, SaveMetricResource.PARAM_VALUE));
		// validators.add(new
		// DateFieldValidator(SaveMetricResource.PARAM_DATETIME));
		return validators;

	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public final ResourceResponse processRequest(final PageParameters parameters) {

		try {
			final StringValue meterIdParam = parameters.get(PARAM_ID);
			final StringValue dateParam = parameters.get(PARAM_DATETIME);
			final StringValue valueParam = parameters.get(PARAM_VALUE);

			Date dt = DateUtils.truncate(new Date(), Calendar.SECOND);

			if (!dateParam.isEmpty()) {
				dt = new Date(dateParam.toLong());
			}

			final Long meterId = meterIdParam.toLongObject();
			final Meter meter = WicketApplication.get().getManagerFactory().getMeterManager().findById(meterId);

			if (meter != null) {

				WicketApplication.get().getManagerFactory().getMetricManager().saveOrUpdate(meter, dt,
						valueParam.toDoubleObject());
				return newJsonResourceResponse(200, "{}");

			} else {

				final Map<String, Object> vars = Maps.newConcurrentMap();
				vars.put("type", "meter");
				vars.put("id", meterId);

				return newErrorResponse(400,
						new APIValidationError(this.getClass(),
								Arrays.asList("validation.does_not_exist"), vars, meterIdParam.toString()).getErrorMessage()
						);

			}

		} catch (final StringValueConversionException e) {
			return newErrorResponse(400, e.getMessage());
		}

	}

}
