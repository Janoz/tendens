package nb.tendens.interfaces.api;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import nb.tendens.interfaces.api.validators.APIValidationError;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.AbstractResource;
import org.apache.wicket.validation.IValidationError;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.Validatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nb.tendens.infrastructure.utils.JsonUtilities;

/**
 * AbstractAPIResource.
 *
 * @author N. Bulthuis
 *
 */
public abstract class AbstractAPIResource extends AbstractResource {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AbstractAPIResource.class);

	/**
	 * Get the pageParameter validators.
	 *
	 * @return a list of validator.
	 */
	public abstract List<IValidator<PageParameters>> getValidators();

	/**
	 * Create a new JSON resource response with the specified http status and
	 * message.
	 *
	 * @param httpStatus
	 *            the http status.
	 * @param message
	 *            the message.
	 * @return resource response.
	 */
	public final ResourceResponse newJsonResourceResponse(final int httpStatus, final String message) {

		final ResourceResponse resourceResponse = new ResourceResponse();
		resourceResponse.setContentType("application/json");
		resourceResponse.setTextEncoding("utf-8");

		resourceResponse.setWriteCallback(new WriteCallback() {
			@Override
			public void writeData(final Attributes attributes) throws IOException {

				try (final OutputStream out = attributes.getResponse().getOutputStream()) {
					out.write(message.getBytes());
				}
			}
		});
		resourceResponse.setStatusCode(httpStatus);

		return resourceResponse;
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	protected final ResourceResponse newResourceResponse(final Attributes attributes) {

		LOG.debug("API.REQUEST: {}", attributes);

		final PageParameters parameters = attributes.getParameters();

		final Validatable<PageParameters> validatable = new Validatable<>();
		validatable.setValue(parameters);

		for (final IValidator<PageParameters> validator : getValidators()) {
			validator.validate(validatable);
		}

		LOG.debug("API.VALIDATION: {}", validatable.isValid());

		ResourceResponse response = null;

		if (validatable.isValid()) {
			response = processRequest(attributes.getParameters());
		} else {
			response = newErrorResponse(400, validatable.getErrors());
		}
		return response;
	}

	protected ResourceResponse newErrorResponse(int statusCode, List<IValidationError> errors) {
		StringBuilder errorMessage = new StringBuilder();
		for (IValidationError error : errors) {
			errorMessage.append(", ");
			errorMessage.append(((APIValidationError)error).getErrorMessage());
		}
		String message = errorMessage.substring(2).toString();
		return newErrorResponse(statusCode, message);
	}

	protected ResourceResponse newErrorResponse(int statusCode, String message) {
		final ResourceResponse resourceResponse = new ResourceResponse();
		resourceResponse.setError(statusCode, message);
		return resourceResponse;
	}

	/**
	 * Process the request.
	 *
	 * @param parameters
	 *            the parameters.
	 * @return the resource response.
	 */
	public abstract ResourceResponse processRequest(final PageParameters parameters);

}
