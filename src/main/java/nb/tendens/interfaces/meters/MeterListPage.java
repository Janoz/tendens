package nb.tendens.interfaces.meters;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import nb.tendens.application.meters.Meter;
import nb.tendens.interfaces.WicketApplication;
import nb.tendens.interfaces.components.StylishFeedbackPanel;
import nb.tendens.interfaces.home.BasePage;

/**
 * MeterPage.
 *
 * @author N. Bulthuis
 *
 * @since 1.0
 *
 */
public class MeterListPage extends BasePage {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Filter Model.
	 */
	private final IModel<String> filter = Model.of("");

	/**
	 * Constructor.
	 */
	public MeterListPage() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		final Form<Void> filterForm = new Form<Void>("filterForm");

		filterForm.add(new StylishFeedbackPanel("feedback"));
		filterForm.add(new TextField<String>("filter", filter));

		filterForm.add(new MeterList("meterList", filter));
		add(filterForm);

		final IModel<Meter> meterModel = Model.of(new Meter());
		final Form<Meter> addForm = new Form<Meter>("addMeterForm", meterModel) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			/*
			 * (non-Javadoc)
			 *
			 * @see org.apache.wicket.markup.html.form.Form#onSubmit()
			 */
			@Override
			protected void onSubmit() {
				WicketApplication.get().getManagerFactory().getMeterManager().saveOrUpdate(getModelObject());
				success(getLocalizer().getString("meter.save.success", this, meterModel));

				setModelObject(new Meter());
			}
		};

		addForm.add(new EditMeter("editMeter", meterModel));
		add(addForm);
	}
}
