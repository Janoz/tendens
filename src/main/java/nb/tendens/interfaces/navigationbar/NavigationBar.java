package nb.tendens.interfaces.navigationbar;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.link.Link;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;

import nb.tendens.interfaces.charts.ChartListPage;
import nb.tendens.interfaces.dashboards.DashboardListPage;
import nb.tendens.interfaces.home.HomePage;
import nb.tendens.interfaces.meters.MeterListPage;

/**
 * Navigation Bar.
 *
 * @author N. Bulthuis
 *
 */
public class NavigationBar extends Panel {
	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 *
	 * @param id
	 *            markupId.
	 */
	public NavigationBar(final String id) {
		super(id);
		setOutputMarkupId(true);
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		final NavigationLink<HomePage> homePage = new NavigationLink<HomePage>("homePage", HomePage.class,
				new ResourceModel("navigation.homePage")) {

			/**
			 * Serial ID.
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void applyOnLink(final Link<HomePage> link) {
				super.applyOnLink(link);
				link.add(AttributeModifier.append("class", "navbar-brand"));
			}
		};

		add(homePage);
		add(new NavigationLink<>("chartListPage", ChartListPage.class, new ResourceModel("navigation.chartListPage"))
				.setAutoEnable(true));
		add(new NavigationLink<>("meterListPage", MeterListPage.class, new ResourceModel("navigation.meterListPage"))
				.setAutoEnable(true));
		add(new NavigationLink<>("dashboardListPage", DashboardListPage.class,
				new ResourceModel("navigation.dashboardListPage")).setAutoEnable(true));

	}

}
