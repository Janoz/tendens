package nb.tendens.interfaces.navigationbar;

import org.apache.wicket.AttributeModifier;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * NavigationLink.
 *
 * @author N. Bulthuis
 *
 */
public class NavigationLink<T> extends Panel {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The Link.
	 */
	private final BookmarkablePageLink<T> link;

	/**
	 * LabelModel.
	 */
	private final IModel<String> labelModel;

	/**
	 * Indicates if autoenable should be used.
	 */
	private boolean autoEnable;

	/**
	 * Create a new NavigationLink.
	 *
	 * @param markupId
	 *            the markup id.
	 * @param pageClass
	 *            the pageclass to link to.
	 * @param labelModel
	 *            the label model.
	 * @param <C>
	 *            class of the pageClass.
	 */
	public <C extends Page> NavigationLink(final String markupId, final Class<C> pageClass,
			final IModel<String> labelModel) {
		super(markupId);

		link = new BookmarkablePageLink<T>("link", pageClass);
		this.labelModel = labelModel;
	}

	/**
	 * Get an Optional CSS class for the Link portion.
	 *
	 * @param applyToLink
	 *            the link to apply styles on.
	 */
	public void applyOnLink(final Link<T> applyToLink) {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.Component#onConfigure()
	 */
	@Override
	protected final void onConfigure() {
		super.onConfigure();

		if (autoEnable) {

			if (link.linksTo(getPage())) {
				add(AttributeModifier.append("class", "active"));
			} else {
				add(AttributeModifier.remove("class"));
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.apache.wicket.MarkupContainer#onInitialize()
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		add(link);

		link.add(new Label("label", labelModel));

		applyOnLink(link);
	}

	/**
	 * Set AutoEnable.
	 *
	 * @param autoEnable
	 *            true or false.
	 * @return the navigation link.
	 */
	public final NavigationLink<T> setAutoEnable(final boolean autoEnable) {
		this.autoEnable = autoEnable;
		return this;
	}

}
