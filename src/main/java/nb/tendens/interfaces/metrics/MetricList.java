package nb.tendens.interfaces.metrics;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.ResourceModel;

import nb.tendens.application.meters.Meter;
import nb.tendens.application.metrics.Metric;
import nb.tendens.interfaces.components.dataproviders.MetricDataProvider;

/**
 * MetricList.
 *
 * @author N. Bulthuis
 *
 */
public class MetricList extends Panel {

	/**
	 * ActionPanel.
	 *
	 * @author N. Bulthuis
	 *
	 */
	private class ActionPanel extends Panel {

		/**
		 * Serial ID.
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * Constructor.
		 *
		 * @param markupId
		 *            markupid.
		 * @param model
		 *            the model.
		 */
		public ActionPanel(final String markupId, final IModel<Metric> model) {
			super(markupId, model);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected final void onInitialize() {
			super.onInitialize();

			add(new DeleteMetricLink("delete", (IModel<Metric>) getDefaultModel()));
		}
	}

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Meter Model.
	 */
	private final IModel<Meter> model;

	/**
	 * Constructor.
	 *
	 * @param markupId
	 *            markupId.
	 */
	public MetricList(final String markupId, final IModel<Meter> model) {
		super(markupId);

		this.model = model;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected final void onInitialize() {
		super.onInitialize();

		final MetricDataProvider dataProvider = new MetricDataProvider(model);

		final List<IColumn<Metric, String>> columns = new ArrayList<>();
		columns.add(new PropertyColumn<Metric, String>(new ResourceModel("label.dateTime"), "dateTime"));
		columns.add(new PropertyColumn<Metric, String>(new ResourceModel("label.value"), "value"));

		columns.add(new AbstractColumn<Metric, String>(new ResourceModel("label.actions")) {
			/**
			 *
			 */
			private static final long serialVersionUID = 1L;

			/*
			 * (non-Javadoc)
			 *
			 * @see
			 * org.apache.wicket.extensions.markup.html.repeater.data.table.
			 * AbstractColumn#getCssClass()
			 */
			@Override
			public String getCssClass() {
				return "text-right";
			}

			@Override
			public void populateItem(final Item<ICellPopulator<Metric>> cellItem, final String componentId,
					final IModel<Metric> model) {
				cellItem.add(new ActionPanel(componentId, model));
			}
		});

		final DefaultDataTable<Metric, String> metricList = new DefaultDataTable<Metric, String>("metricList", columns,
				dataProvider, 25);
		add(metricList);

	}

}
