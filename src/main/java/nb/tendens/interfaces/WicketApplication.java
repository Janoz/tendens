package nb.tendens.interfaces;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.wicket.Application;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.apache.wicket.Session;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;

import nb.tendens.application.shared.ManagerFactory;
import nb.tendens.application.shared.ManagerFactorySql2o;
import nb.tendens.infrastructure.configuration.Configuration;
import nb.tendens.infrastructure.configuration.PropertyConfiguration;
import nb.tendens.infrastructure.utils.ResourceLoader;
import nb.tendens.interfaces.api.GetMeterInfoResource;
import nb.tendens.interfaces.api.SaveMetricResource;
import nb.tendens.interfaces.charts.ChartListPage;
import nb.tendens.interfaces.home.HomePage;
import nb.tendens.interfaces.meters.MeterListPage;

/**
 * Application.
 *
 * @author N. Bulthuis
 *
 * @since 1.0
 */
public class WicketApplication extends WebApplication {

	/**
	 * Get the Application.
	 *
	 * @return the wicket application.
	 */
	public static WicketApplication get() {
		return (WicketApplication) Application.get();
	}

	/**
	 * Project Properties.
	 */
	private final Properties projectProperties = new Properties();

	/**
	 * Configuration.
	 */
	private Configuration configuration;

	/**
	 * ManagerFactory.
	 */
	private ManagerFactory managerFactory;

	/**
	 * Get the Configuration.
	 *
	 * @return get the configuration.
	 */
	public Configuration getConfiguration() {

		if (configuration == null) {
			String configFile = getConfigFile();

			configuration = new PropertyConfiguration(new File(configFile));
		}

		return configuration;
	}

	private String getConfigFile() {
		String file = getServletContext().getInitParameter(PropertyConfiguration.KEY_CONFIGURATION_FILE);
		if (file == null) {
			file = System.getenv(PropertyConfiguration.KEY_CONFIGURATION_FILE);
		}
		if (file == null) {
			throw new RuntimeException("Unable to retrieve configuration file location");
		}
		return file;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public final Class<? extends WebPage> getHomePage() {
		return HomePage.class;
	}

	public ManagerFactory getManagerFactory() {
		return managerFactory;
	}

	public Properties getProjectProperties() {
		return projectProperties;
	}

	/**
	 * Initialize the Wicket Application.
	 *
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public final void init() {
		super.init();

		loadProjectProperties();

		managerFactory = new ManagerFactorySql2o(getConfiguration());

		final String defaultTimeZone = getConfiguration().getDefaultTimeZone();
		if (defaultTimeZone != null) {
			final TimeZone tz = TimeZone.getTimeZone(defaultTimeZone);
			TimeZone.setDefault(tz);
		}

		getMarkupSettings().setStripWicketTags(true);

		mountResource("/api/metric", SaveMetricResource.newResourceReference());
		mountResource("/api/meter", GetMeterInfoResource.newResourceReference());

		mountPage("meters", MeterListPage.class);
		mountPage("charts", ChartListPage.class);

	}

	/**
	 * Load the Project Properties.
	 */
	private void loadProjectProperties() {

		try (StringReader r = new StringReader(ResourceLoader.toStringQuietly("project.properties"))) {
			projectProperties.load(r);

			projectProperties.list(System.out);

		} catch (final IOException e) {
			throw new WicketRuntimeException(e);
		}

	}

	/**
	 * {@inheritDoc}
	 *
	 * @see org.apache.wicket.authroles.authentication.AuthenticatedWebApplication#newSession(org.apache.wicket.request.Request,
	 *      org.apache.wicket.request.Response)
	 */
	@Override
	public final Session newSession(final Request request, final Response response) {

		final Session session = new WicketSession(request);
		session.setLocale(request.getLocale());
		return session;
	}

}
