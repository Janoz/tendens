package nb.tendens.application.schemachanges;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Date;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

/**
 * SchemaChangesSql2o.
 *
 * @author N. Bulthuis
 *
 */
public class SchemaChanges {

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(SchemaChanges.class);

	/**
	 * Sql2o.
	 */
	private final Sql2o sql2o;

	/**
	 * SchemaChanges table.
	 */
	private final String schemaChangesTable = "schemachanges";

	/**
	 * Constructor.
	 *
	 * @param sql2o
	 *            the sql2o session.
	 */
	public SchemaChanges(final Sql2o sql2o) {
		this.sql2o = sql2o;

	}

	/**
	 * Get the ID of the last applied SchemaChange.
	 *
	 * @return the id of the last applied schemachange.
	 */
	public final Long getLastAppliedSchemaChange() {

		Long lastAppliedSchemaChange = 0L;

		try (Connection cn = sql2o.beginTransaction()) {

			try {
				lastAppliedSchemaChange = cn.createQuery("select MAX(id) from " + getSchemaChangesTable())
						.executeScalar(Long.class);
			} catch (final Sql2oException e) {
				LOG.error("Caught error while querying schemachanges table, most likely it does not exist yet: "
						+ e.getMessage());
			}

			cn.commit();
		}

		return lastAppliedSchemaChange;
	}

	/**
	 * @return the schemaChangesTable
	 */
	protected final String getSchemaChangesTable() {
		return schemaChangesTable;
	}

	/**
	 * Perform pending schemachanges.
	 */
	public final void performPendingSchemaChanges() {

		final Long lastAppliedSchemaChange = getLastAppliedSchemaChange();

		final Set<SchemaChange> schemaChanges = new SchemaChangeLoader(this.getClass().getPackage().getName())
				.getSchemaChanges();
		try (Connection cn = sql2o.beginTransaction()) {

			for (final SchemaChange schemaChange : schemaChanges) {

				if (schemaChange.getId() > lastAppliedSchemaChange) {
					LOG.debug("Executing SchemaChange: {} - {}", schemaChange.getId(),
							schemaChange.getClass().getSimpleName());

					LOG.debug("Script: {}", schemaChange.getScript());

					cn.createQuery(schemaChange.getScript()).executeUpdate();
					cn.createQuery("INSERT INTO " + getSchemaChangesTable()
							+ " (id, applied_at, description) VALUES (:id, :applied_at, :description)")
							.addParameter("id", schemaChange.getId()).addParameter("applied_at", new Date())
							.addParameter("description", schemaChange.getClass().getSimpleName()).executeUpdate();
				}
			}
			cn.commit();
		}
	}

}
