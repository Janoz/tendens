package nb.tendens.application.schemachanges;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

/**
 * SC003CreateTableMetrics.
 *
 * @author N. Bulthuis
 *
 */
public class SC003CreateTableMetrics extends SchemaChange {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final Long getId() {

		return 3L;

	};

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final String getScript() {
		// @formatter:off
		return "CREATE TABLE metrics ( "
				+ " id BIGINT NOT NULL AUTO_INCREMENT"
				+ ", meter_id BIGINT NOT NULL"
				+ ", datetime DATETIME NOT NULL"
				+ ", value DOUBLE NOT NULL"
				+ ", PRIMARY KEY (id)"
				+ ", UNIQUE (meter_id, datetime)"
				+ ", CONSTRAINT fk_meters FOREIGN KEY (meter_id) REFERENCES meters (id) ON DELETE CASCADE ON UPDATE RESTRICT"
				+ ")";
		// @formatter:on
	}

}
