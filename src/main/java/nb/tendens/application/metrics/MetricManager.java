package nb.tendens.application.metrics;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Date;
import java.util.List;

import nb.tendens.application.charts.datasources.DataSource;
import nb.tendens.application.charts.series.AbstractSerie;
import nb.tendens.application.meters.Meter;

/**
 * MetricManager.
 *
 * @author N. Bulthuis
 *
 */
public interface MetricManager {

	/**
	 * Get the number of meters.
	 *
	 * @param meter
	 *            the meter for which to count the metrics.
	 * @return the number of meters.
	 */
	long count(final Meter meter);

	/**
	 * Delete a Metric.
	 *
	 * @param metric
	 *            the metric to delete.
	 */
	void delete(final Metric metric);

	/**
	 * Find a Metric by id.
	 *
	 * @param id
	 *            the id of the metric.
	 * @return the meter or null.
	 */
	Metric findById(final Long id);

	/**
	 * Find Metrics for the specified plotBand.
	 *
	 * @param plotBand
	 *            the plotBand.
	 * @return a list of metrics.
	 */
	List<SingleCalculatedMetric> findForDataSource(final DataSource dataSource, final Date from, final Date until);

	/**
	 * Find Metrics for the specified serie.
	 *
	 * @param serie
	 *            the serie.
	 * @return a list of metrics.
	 */
	List<CalculatedMetric> findForSerie(AbstractSerie serie, final Date from, final Date until);

	/**
	 * Save or update a metric for the specified meter.
	 *
	 * @param meter
	 *            the meter.
	 * @param date
	 *            the date of the metric.
	 * @param value
	 *            the value of the metric.
	 */
	void saveOrUpdate(final Meter meter, final Date date, final Double value);

	/**
	 * Get a subList.
	 *
	 * @param meter
	 *            the meter.
	 * @param offset
	 *            the offset.
	 * @param count
	 *            the count.
	 * @return a sublist of metrics.
	 */
	List<Metric> subList(final Meter meter, final long offset, final long count);

}
