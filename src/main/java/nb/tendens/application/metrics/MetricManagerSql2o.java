package nb.tendens.application.metrics;

import java.util.Comparator;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import edu.emory.mathcs.backport.java.util.Collections;
import nb.tendens.application.charts.datasources.DataSource;
import nb.tendens.application.charts.datasources.Reductor;
import nb.tendens.application.charts.series.AbstractSerie;
import nb.tendens.application.charts.series.AreaRangeSerie;
import nb.tendens.application.charts.series.BasicSerie;
import nb.tendens.application.meters.Meter;
import nb.tendens.infrastructure.utils.Pair;
import nb.tendens.infrastructure.utils.ResourceLoader;

/**
 * MetricsManager.
 *
 * @author N. Bulthuis
 *
 */

public class MetricManagerSql2o implements MetricManager {

	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MetricManagerSql2o.class);

	/**
	 * Sql2o.
	 */
	private final Sql2o sql2o;

	/**
	 * Cosntructor.
	 *
	 * @param sql2o
	 *            the sql2o.
	 */
	public MetricManagerSql2o(final Sql2o sql2o) {
		this.sql2o = sql2o;
	}

	/**
	 * Push the values forward through the reductor.
	 *
	 * @param unpushed
	 *            unpushed values.
	 * @param dataSource
	 *            the dataSource.
	 * @param from
	 *            from date.
	 * @param until
	 *            until date.
	 * @return
	 */
	private List<SingleCalculatedMetric> addMissingEndValues(final List<SingleCalculatedMetric> orig,
			final Reductor reductor, final Date until) {

		final List<SingleCalculatedMetric> modified = Lists.newArrayList(orig);

		if (modified.size() > 0) {
			final SingleCalculatedMetric last = modified.get(modified.size() - 1);
			Date startDate = last.getDateTime();

			while (reductor.getNextReductorDate(startDate).before(until)) {

				startDate = reductor.getNextReductorDate(startDate);

				final SingleCalculatedMetric copy = last.copy();
				copy.setDateTime(startDate);

				modified.add(copy);
			}
		}

		return modified;

	}

	/**
	 * Add Missing Intermediate Values.
	 *
	 * @param orig
	 * @param reductor
	 * @return
	 */
	private List<SingleCalculatedMetric> addMissingIntermediateValues(final List<SingleCalculatedMetric> orig,
			final Reductor reductor) {

		final List<SingleCalculatedMetric> modified = Lists.newArrayList();

		/*
		 * Step 1: add all intermediate values.
		 */
		for (final Pair<SingleCalculatedMetric> pair : Pair.over(orig)) {

			final SingleCalculatedMetric start = pair.first;
			final SingleCalculatedMetric end = pair.second;

			modified.add(start);

			Date startDate = start.getDateTime();

			if (end != null) {
				while (reductor.getNextReductorDate(startDate).before(end.getDateTime())) {

					startDate = reductor.getNextReductorDate(startDate);

					final SingleCalculatedMetric copy = start.copy();
					copy.setDateTime(startDate);

					modified.add(copy);
				}
			}
		}

		if (orig != null && orig.size() > 0) {
			modified.add(orig.get(orig.size() - 1));
		}

		return modified;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long count(final Meter meter) {

		try (Connection cn = sql2o.open()) {
			return cn.createQuery("SELECT COUNT(*) FROM metrics WHERE meter_id = :meter_id")
					.addParameter("meter_id", meter.getId()).executeScalar(Long.class);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void delete(final Metric meter) {

		try (Connection cn = sql2o.beginTransaction()) {

			final Query q = cn.createQuery("DELETE FROM metrics WHERE id = :id");
			q.addParameter("id", meter.getId());
			q.executeUpdate();

			cn.commit();
		}
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public Metric findById(final Long id) {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
					+ "id "
					+ ", datetime "
					+ ", value "
					+ "FROM "
					+ "	metrics "
					+ "WHERE id = :id "
				+ ";";
			// @formatter:on

			return cn.createQuery(sql).addParameter("id", id).executeAndFetchFirst(Metric.class);
		}

	}

	/**
	 * Calculate the metrics for the specified datasource.
	 *
	 * @param dataSource
	 *            the datasource.
	 * @return a list of calculated metrics.
	 */
	@Override
	public final List<SingleCalculatedMetric> findForDataSource(final DataSource dataSource, final Date from,
			final Date until) {

		List<SingleCalculatedMetric> result = Lists.newArrayList();

		switch (dataSource.getReductor()) {
			case YEAR:
				result.addAll(findForDataSourceByYear(dataSource, from, until));
			case MONTH:
				result.addAll(findForDataSourceByMonth(dataSource, from, until));
			default:
				result.addAll(findForDataSourceRound(dataSource, from, until));
		}

		if (dataSource.isAddMissingIntermediateValues()) {
			result = addMissingIntermediateValues(result, dataSource.getReductor());
		}
		if (dataSource.isAddMissingEndValues()) {
			result = addMissingEndValues(result, dataSource.getReductor(), until);
		}

		return result;

	}

	/**
	 * Calculate the metrics for the specified datasource.
	 *
	 * @param dataSource
	 *            the datasource.
	 * @return a list of calculated metrics.
	 */
	public final List<SingleCalculatedMetric> findForDataSourceByMonth(final DataSource dataSource, final Date from,
			final Date until) {

		try (Connection cn = sql2o.beginTransaction()) {

			final String sql = ResourceLoader.toStringQuietly("sql2o/metric/findForDataSourceByMonth.sql");
			final Query q = cn.createQuery(sql);

			q.addParameter("meter_id", dataSource.getMeter().getId());
			q.addParameter("from", from);
			q.addParameter("until", until);
			q.addParameter("aggregator", dataSource.getAggregator().name());

			return q.executeAndFetch(SingleCalculatedMetric.class);
		}
	}

	/**
	 * Calculate the metrics for the specified datasource.
	 *
	 * @param dataSource
	 *            the datasource.
	 * @return a list of calculated metrics.
	 */
	public final List<SingleCalculatedMetric> findForDataSourceByYear(final DataSource dataSource, final Date from,
			final Date until) {
		try (Connection cn = sql2o.beginTransaction()) {

			final String sql = ResourceLoader.toStringQuietly("sql2o/metric/FindForDataSourceByYear.sql");
			final Query q = cn.createQuery(sql);

			q.addParameter("meter_id", dataSource.getMeter().getId());
			q.addParameter("from", from);
			q.addParameter("until", until);
			q.addParameter("aggregator", dataSource.getAggregator().name());

			return q.executeAndFetch(SingleCalculatedMetric.class);
		}
	}

	/**
	 * Calculate the metrics for the specified datasource.
	 *
	 * @param dataSource
	 *            the datasource.
	 * @return a list of calculated metrics.
	 */
	public final List<SingleCalculatedMetric> findForDataSourceRound(final DataSource dataSource, final Date from,
			final Date until) {

		try (Connection cn = sql2o.beginTransaction()) {

			final String sql = ResourceLoader.toStringQuietly("sql2o/metric/findForDataSource.sql");
			final Query q = cn.createQuery(sql);

			q.addParameter("meter_id", dataSource.getMeter().getId());
			q.addParameter("roundToHour", dataSource.getReductor().getRoundToHour());
			q.addParameter("roundToMinute", dataSource.getReductor().getRoundToMinute());
			q.addParameter("roundToSecond", dataSource.getReductor().getRoundToSecond());
			q.addParameter("from", from);
			q.addParameter("until", until);
			q.addParameter("aggregator", dataSource.getAggregator().name());

			return q.executeAndFetch(SingleCalculatedMetric.class);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * nb.tendens.application.metrics.MetricManager#findForDataSource(nb.tendens
	 * .application.charts.series.Serie)
	 */
	@Override
	public final List<CalculatedMetric> findForSerie(final AbstractSerie serie, final Date from, final Date until) {

		final List<CalculatedMetric> metrics = Lists.newArrayList();

		switch (serie.getSerieType()) {

			case AREARANGE:
			case AREASPLINERANGE:

				final AreaRangeSerie arSerie = (AreaRangeSerie) serie;
				final DataSource high = arSerie.getDataSourceHigh();
				final DataSource low = arSerie.getDataSourceLow();

				final List<SingleCalculatedMetric> metricsHigh = findForDataSource(high, from, until);
				final List<SingleCalculatedMetric> metricsLow = findForDataSource(low, from, until);

				metrics.addAll(mergeAreaRangeDataSources(metricsHigh, metricsLow));

				break;
			default: {

				final BasicSerie dSerie = (BasicSerie) serie;
				final DataSource ds = dSerie.getDataSource();

				metrics.addAll(findForDataSource(ds, from, until));

			}
		}
		return metrics;
	}

	/**
	 * Find for area range datasources, those with an high and low plot line.
	 *
	 * @param high
	 *            the high datasource.
	 * @param low
	 *            the low datasource.
	 * @param from
	 *            from.
	 * @param until
	 *            until.
	 * @return the result.
	 */
	public final List<HighLowCalculatedMetric> mergeAreaRangeDataSources(final List<SingleCalculatedMetric> highMetrics,
			final List<SingleCalculatedMetric> lowMetrics) {

		final Map<Date, HighLowCalculatedMetric> merger = Maps.newConcurrentMap();

		/*
		 * Add all metrics of the high datasource.
		 */
		for (final SingleCalculatedMetric highMetric : highMetrics) {

			final HighLowCalculatedMetric hlMetric = new HighLowCalculatedMetric();
			hlMetric.setDateTime(highMetric.getDateTime());
			hlMetric.setHigh(highMetric);
			merger.put(hlMetric.getDateTime(), hlMetric);
		}

		/*
		 * Add the low metric if a high metric exists.
		 */
		for (final SingleCalculatedMetric lowMetric : lowMetrics) {

			if (merger.containsKey(lowMetric.getDateTime())) {

				final HighLowCalculatedMetric hlMetric = merger.get(lowMetric.getDateTime());
				hlMetric.setLow(lowMetric);
			} else {
				final HighLowCalculatedMetric hlMetric = new HighLowCalculatedMetric();
				hlMetric.setDateTime(lowMetric.getDateTime());
				hlMetric.setLow(lowMetric);
				merger.put(hlMetric.getDateTime(), hlMetric);
			}
		}

		final List<HighLowCalculatedMetric> result = Lists.newArrayList(merger.values());
		Collections.sort(result, new Comparator<HighLowCalculatedMetric>() {
			@Override
			public int compare(final HighLowCalculatedMetric o1, final HighLowCalculatedMetric o2) {
				return o1.getDateTime().compareTo(o2.getDateTime());
			}
		});
		return result;
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public final void saveOrUpdate(final Meter meter, final Date datetime, final Double value) {

		try (Connection cn = sql2o.beginTransaction()) {

			try {

				LOG.info("Metric.SAVE: {}, {}, {}", meter, datetime, value);

				final Query saveQuery = cn.createQuery(
						"INSERT INTO metrics (meter_id, datetime, value) VALUES (:meter_id, :datetime, :value)");
				saveQuery.addParameter("meter_id", meter.getId());
				saveQuery.addParameter("datetime", datetime);
				saveQuery.addParameter("value", value);

				saveQuery.executeUpdate();

			} catch (final Sql2oException e) {

				/*
				 * If the insert fails, we might need to update it with a new
				 * value. So let's test if it actually exists, if it does update
				 * it, if not it is probably another error and we will rethrow
				 * it.
				 */
				final Query existsQuery = cn.createQuery(
						"SELECT COUNT(*) FROM metrics WHERE meter_id = :meter_id AND datetime = :datetime");
				existsQuery.addParameter("meter_id", meter.getId());
				existsQuery.addParameter("datetime", datetime);

				final Integer existsCount = existsQuery.executeScalar(Integer.class);

				if (BooleanUtils.toBoolean(existsCount)) {

					final Query updateQuery = cn.createQuery(
							"UPDATE metrics SET value = :value WHERE meter_id = :meter_id AND datetime = :datetime");
					updateQuery.addParameter("meter_id", meter.getId());
					updateQuery.addParameter("datetime", datetime);
					updateQuery.addParameter("value", value);

					updateQuery.executeUpdate();

					LOG.debug("Metric.UPDATE: {}, {}, {}", meter, datetime, value);

				} else {
					throw e;
				}

			}
			cn.commit();

		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.metrics.MetricManager#iterate(long, long)
	 */
	@Override
	public final List<Metric> subList(final Meter meter, final long offset, final long count) {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "id "
				+ ", datetime "
				+ ", value "
				+ "FROM "
				+ "	metrics "
				+ "WHERE meter_id = :meter_id "
				+ "ORDER BY datetime DESC "
				+ "LIMIT :offset, :count"
				+ ";";
			// @formatter:on
			return cn.createQuery(sql).addParameter("meter_id", meter.getId()).addParameter("offset", offset)
					.addParameter("count", count).executeAndFetch(Metric.class);
		}
	}

}
