package nb.tendens.application.metrics;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Date;

/**
 * CalculatedMetric.
 *
 * @author N. Bulthuis
 *
 */
public class HighLowCalculatedMetric implements CalculatedMetric {

	/**
	 * DateTime.
	 */
	private Date dateTime;

	/**
	 * Low.
	 */
	private SingleCalculatedMetric low;

	/**
	 * High.
	 */
	private SingleCalculatedMetric high;

	/**
	 * @return the dateTime
	 */
	public Date getDateTime() {
		return dateTime;
	}

	/**
	 * @return the high
	 */
	public SingleCalculatedMetric getHigh() {
		return high;
	}

	/**
	 * @return the low
	 */
	public SingleCalculatedMetric getLow() {
		return low;
	}

	/**
	 * @param dateTime
	 *            the dateTime to set
	 */
	public void setDateTime(final Date dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * @param high
	 *            the high to set
	 */
	public void setHigh(final SingleCalculatedMetric high) {
		this.high = high;
	}

	/**
	 * @param low
	 *            the low to set
	 */
	public void setLow(final SingleCalculatedMetric low) {
		this.low = low;
	}
}
