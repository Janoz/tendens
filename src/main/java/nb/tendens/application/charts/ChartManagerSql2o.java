package nb.tendens.application.charts;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import com.google.common.collect.Lists;

/**
 * ChartManager.
 *
 * @author N. Bulthuis
 *
 */
public class ChartManagerSql2o implements ChartManager {

	/**
	 * Sql2o.
	 */
	private final Sql2o sql2o;

	/**
	 * Constructor.
	 *
	 * @param sql2o
	 *            the sql2o.
	 */
	public ChartManagerSql2o(final Sql2o sql2o) {
		this.sql2o = sql2o;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.charts.ChartManager#iterate(long, long)
	 */
	@Override
	public final List<ChartConfiguration> all() {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "id "
				+ ", config "
				+ "FROM "
				+ "	charts "
				+ " ORDER BY name "
				+ ";";
			// @formatter:on

			final List<ChartConfigurationPO> pos = cn.createQuery(sql).executeAndFetch(ChartConfigurationPO.class);

			final List<ChartConfiguration> charts = Lists.newArrayList();
			for (final ChartConfigurationPO po : pos) {
				charts.add(po.toChartConfiguration());
			}

			return charts;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long count() {

		try (Connection cn = sql2o.open()) {
			return cn.createQuery("SELECT COUNT(*) FROM charts").executeScalar(Long.class);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long count(final String filter) {

		if (StringUtils.isNoneBlank(filter)) {

			try (Connection cn = sql2o.open()) {
				return cn.createQuery("SELECT COUNT(*) FROM charts WHERE name LIKE :filter")
						.addParameter("filter", "%" + filter + "%").executeScalar(Long.class);
			}
		} else {
			return count();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void delete(final ChartConfiguration chart) {

		try (Connection cn = sql2o.beginTransaction()) {

			final Query q = cn.createQuery("DELETE FROM charts WHERE id = :id");
			q.addParameter("id", chart.getId());
			q.executeUpdate();

			cn.commit();
		}
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public ChartConfiguration findById(final Long id) {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "id "
				+ ", config "
				+ "FROM "
				+ "	charts "
				+ "WHERE id = :id "
				+ ";";
			// @formatter:on

			final ChartConfigurationPO po = cn.createQuery(sql).addParameter("id", id)
					.executeAndFetchFirst(ChartConfigurationPO.class);

			if (po != null) {
				return po.toChartConfiguration();
			} else {
				return null;
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void saveOrUpdate(final ChartConfiguration chart) {

		try (Connection cn = sql2o.beginTransaction()) {

			Query q = null;

			if (chart.getId() != null) {

				q = cn.createQuery("UPDATE charts SET config = :config, name = :name WHERE id = :id");

			} else {

				q = cn.createQuery("INSERT INTO charts (id, name, config) VALUES (:id, :name, :config)");

			}
			q.addParameter("id", chart.getId());
			q.addParameter("name", chart.getName());
			q.addParameter("config", new ChartConfigurationPO(chart).getConfig());
			q.executeUpdate();

			cn.commit();

		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.charts.ChartManager#subList(long, long)
	 */
	@Override
	public List<ChartConfiguration> subList(final String filter, final long offset, final long count) {

		final List<ChartConfiguration> result = withFilter(filter);

		return result.subList((int) offset, Math.min((int) (offset + count), result.size()));

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.charts.ChartManager#iterate(long, long)
	 */
	public final List<ChartConfiguration> withFilter(final String filter) {

		if (StringUtils.isNoneBlank(filter)) {

			try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "id "
				+ ", config "
				+ "FROM "
				+ "	charts "
				+ " WHERE name LIKE :filter "
				+ " ORDER BY name "
				+ ";";
			// @formatter:on

				final List<ChartConfigurationPO> pos = cn.createQuery(sql).addParameter("filter", "%" + filter + "%")
						.executeAndFetch(ChartConfigurationPO.class);

				final List<ChartConfiguration> charts = Lists.newArrayList();
				for (final ChartConfigurationPO po : pos) {
					charts.add(po.toChartConfiguration());
				}

				return charts;
			}
		} else {
			return all();
		}
	}

}
