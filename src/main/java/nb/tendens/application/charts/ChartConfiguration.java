package nb.tendens.application.charts;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

import nb.tendens.application.shared.AbstractPersistentObject;

/**
 * ChartConfiguration.
 *
 * @author N. Bulthuis
 *
 */
public class ChartConfiguration extends AbstractPersistentObject implements Serializable {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create a new ChartConfiguration.
	 *
	 * @return the ChartConfiguration.
	 */
	public static ChartConfiguration newDefault() {

		final ChartConfiguration cc = new ChartConfiguration();
		cc.addYAxis();
		return cc;

	}

	/**
	 * Name of the chart.
	 */
	private String name;

	/**
	 * From Date.
	 */
	private String from;

	/**
	 * ChartType.
	 */
	private ChartType chartType = ChartType.BASIC;

	/**
	 * Until Date.
	 */
	private String until;

	/**
	 * Height.
	 */
	private String height;

	/**
	 * YAxes.
	 */
	private List<YAxis> yAxes = Lists.newArrayList();

	/**
	 * XAxis.
	 */
	private XAxis xAxis = new XAxis();

	/**
	 * Add an YAxis.
	 */
	public final YAxis addYAxis() {

		final YAxis axis = new YAxis();
		yAxes.add(axis);
		return axis;
	}

	/**
	 * @return the chartType
	 */
	public ChartType getChartType() {
		return chartType;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	public String getHeight() {
		return height;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the until
	 */
	public String getUntil() {
		return until;
	}

	/**
	 * @return the xAxis
	 */
	public XAxis getXAxis() {
		return xAxis;
	}

	/**
	 * @return the yAxes
	 */
	public List<YAxis> getYAxes() {
		return yAxes;
	}

	/**
	 * Remove an YAxis from the yAxes list.
	 *
	 * @param yAxis
	 *            the axis to remove.
	 */
	public void removeYAxis(final YAxis yAxis) {
		yAxes.remove(yAxis);

	}

	/**
	 * @param chartType
	 *            the chartType to set
	 */
	public void setChartType(final ChartType chartType) {
		this.chartType = chartType;
	}

	/**
	 * @param from
	 *            the from to set
	 */
	public void setFrom(final String from) {
		this.from = from;
	}

	public void setHeight(final String height) {
		this.height = height;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param until
	 *            the until to set
	 */
	public void setUntil(final String until) {
		this.until = until;
	}

	/**
	 * @param xAxis
	 *            the xAxis to set
	 */
	public void setXAxis(final XAxis xAxis) {
		this.xAxis = xAxis;
	}

	/**
	 * @param yAxes
	 *            the yAxes to set
	 */
	public void setYAxes(final List<YAxis> yAxes) {
		this.yAxes = yAxes;
	}

}
