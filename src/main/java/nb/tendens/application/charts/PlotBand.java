package nb.tendens.application.charts;

import java.io.Serializable;
import java.util.UUID;

import nb.tendens.application.charts.datasources.DataSource;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

/**
 * PlotLine.
 *
 * @author N. Bulthuis
 *
 */
public class PlotBand implements Serializable {

	/**
	 * Unique id.
	 */
	private final String id = UUID.randomUUID().toString();
	/**
	 * Color.
	 */
	private String color;

	/**
	 * Z-Index.
	 */
	private Integer zIndex;

	/**
	 * Value.
	 */
	private Double onValue;

	/**
	 * Value.
	 */
	private Double offValue;

	/**
	 * Label.
	 */
	private String label;

	/**
	 * DataSource.
	 */
	private DataSource dataSource = new DataSource();

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the to
	 */
	public Double getOffValue() {
		return offValue;
	}

	/**
	 * @return the onValue
	 */
	public Double getOnValue() {
		return onValue;
	}

	/**
	 * @return the zIndex
	 */
	public Integer getZIndex() {
		return zIndex;
	}

	/**
	 * Indicates if this serie is valid.
	 *
	 * @return true if valid.
	 */
	public boolean isValid() {

		return dataSource.getMeter() != null;

	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(final String color) {
		this.color = color;
	}

	/**
	 * @param dataSource
	 *            the dataSource to set
	 */
	public void setDataSource(final DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * @param offValue
	 *            the to to set
	 */
	public void setOffValue(final Double offValue) {
		this.offValue = offValue;
	}

	/**
	 * @param onValue
	 *            the onValue to set
	 */
	public void setOnValue(final Double onValue) {
		this.onValue = onValue;
	}

	/**
	 * @param zIndex
	 *            the zIndex to set
	 */
	public void setZIndex(final Integer zIndex) {
		this.zIndex = zIndex;
	}

}
