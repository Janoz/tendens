package nb.tendens.application.charts;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

import nb.tendens.application.charts.series.AbstractSerie;

/**
 * YAxis.
 *
 * @author N. Bulthuis
 *
 */
public class YAxis implements Serializable {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Series.
	 */
	private List<AbstractSerie> series = Lists.newArrayList();

	/**
	 * PlotLines.
	 */
	private List<PlotLine> plotLines = Lists.newArrayList();

	/**
	 * Opposite.
	 */
	private boolean opposite;

	/**
	 * StartOnTick.
	 */
	private boolean startOnTick = true;

	/**
	 * EndOnTick.
	 */
	private boolean endOnTick = true;

	/**
	 * Label Format.
	 */
	private String labelFormat;

	/**
	 * Offset.
	 */
	private Integer offset;

	/**
	 * YAxis Name.
	 */
	private String name;

	/**
	 * Top.
	 */
	private String top;

	/**
	 * Height.
	 */
	private String height;

	/**
	 * Minimum Extreme.
	 */
	private Double min;

	/**
	 * Maximum Extreme.
	 */
	private Double max;

	/**
	 * Floor.
	 */
	private Double floor;

	/**
	 * Ceiling.
	 */
	private Double ceiling;

	/**
	 * Tick Amount.
	 */
	private Integer tickAmount;

	/**
	 * Constructor.
	 */
	public YAxis() {

	}

	/**
	 * Add a PlotLine.
	 */
	public final void addPlotLine() {
		plotLines.add(new PlotLine());
	}

	/**
	 * Add a serie.
	 *
	 * @param serie
	 *            the serie to add.
	 */
	public final void addSerie(final AbstractSerie serie) {
		series.add(serie);
	}

	public Double getCeiling() {
		return ceiling;
	}

	public Double getFloor() {
		return floor;
	}

	public String getHeight() {
		return height;
	}

	/**
	 * @return the labelFormat
	 */
	public String getLabelFormat() {
		return labelFormat;
	}

	public Double getMax() {
		return max;
	}

	public Double getMin() {
		return min;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public Integer getOffset() {
		return offset;
	}

	/**
	 * @return the plotlines
	 */
	public List<PlotLine> getPlotLines() {
		return plotLines;
	}

	/**
	 * @return the series
	 */
	public final List<AbstractSerie> getSeries() {
		return series;
	}

	public Integer getTickAmount() {
		return tickAmount;
	}

	public String getTop() {
		return top;
	}

	public boolean isEndOnTick() {
		return endOnTick;
	}

	/**
	 * @return the opposite
	 */
	public final boolean isOpposite() {
		return opposite;
	}

	public boolean isStartOnTick() {
		return startOnTick;
	}

	/**
	 * Remove a plotLine from the plotlines list.
	 *
	 * @param plotLine
	 *            the plotLine to remove.
	 */
	public final void removePlotLine(final PlotLine plotLine) {
		plotLines.remove(plotLine);

	}

	/**
	 * Remove a serie from the series list.
	 *
	 * @param serie
	 *            the serie to remove.
	 */
	public final void removeSerie(final AbstractSerie serie) {
		series.remove(serie);

	}

	public void setCeiling(final Double ceiling) {
		this.ceiling = ceiling;
	}

	public void setEndOnTick(final boolean endOnTick) {
		this.endOnTick = endOnTick;
	}

	public void setFloor(final Double floor) {
		this.floor = floor;
	}

	public void setHeight(final String height) {
		this.height = height;
	}

	/**
	 * @param labelFormat
	 *            the labelFormat to set
	 */
	public void setLabelFormat(final String labelFormat) {
		this.labelFormat = labelFormat;
	}

	public void setMax(final Double max) {
		this.max = max;
	}

	public void setMin(final Double min) {
		this.min = min;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	public void setOffset(final Integer offset) {
		this.offset = offset;
	}

	/**
	 * @param opposite
	 *            the opposite to set
	 */
	public final void setOpposite(final boolean opposite) {
		this.opposite = opposite;
	}

	/**
	 * @param plotLines
	 *            the plotLines to set
	 */
	public void setPlotLines(final List<PlotLine> plotLines) {
		this.plotLines = plotLines;
	}

	/**
	 * @param series
	 *            the series to set
	 */
	public void setSeries(final List<AbstractSerie> series) {
		this.series = series;
	}

	public void setStartOnTick(final boolean startOnTick) {
		this.startOnTick = startOnTick;
	}

	public void setTickAmount(final Integer tickAmount) {
		this.tickAmount = tickAmount;
	}

	public void setTop(final String top) {
		this.top = top;
	}

}
