package nb.tendens.application.charts.datasources;

import java.io.Serializable;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import nb.tendens.application.meters.Meter;

/**
 * DataSource.
 *
 * @author N. Bulthuis
 *
 */
public class DataSource implements Serializable {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Meter.
	 */
	private Meter meter;

	/**
	 * Add missing values between the entries.
	 */
	private boolean addMissingIntermediateValues;

	/**
	 * Add missing values from the last entry until the until date.
	 */
	private boolean addMissingEndValues;

	/**
	 * Reductor.
	 */
	private Reductor reductor = Reductor.NONE;

	/**
	 * Aggregator.
	 */
	private Aggregator aggregator = Aggregator.NONE;

	/**
	 * @return the aggregator
	 */
	public Aggregator getAggregator() {
		return aggregator;
	}

	/**
	 * @return the meter
	 */
	public Meter getMeter() {
		return meter;
	}

	/**
	 * @return the reductor
	 */
	public Reductor getReductor() {
		return reductor;
	}

	/**
	 * @return the addMissingEndValues
	 */
	public boolean isAddMissingEndValues() {
		return addMissingEndValues;
	}

	/**
	 * @return the addMissingIntermediateValues
	 */
	public boolean isAddMissingIntermediateValues() {
		return addMissingIntermediateValues;
	}

	/**
	 * @param addMissingEndValues
	 *            the addMissingEndValues to set
	 */
	public void setAddMissingEndValues(final boolean addMissingEndValues) {
		this.addMissingEndValues = addMissingEndValues;
	}

	/**
	 * @param addMissingIntermediateValues
	 *            the addMissingIntermediateValues to set
	 */
	public void setAddMissingIntermediateValues(final boolean addMissingIntermediateValues) {
		this.addMissingIntermediateValues = addMissingIntermediateValues;
	}

	/**
	 * @param aggregator
	 *            the aggregator to set
	 */
	public void setAggregator(final Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	/**
	 * @param meter
	 *            the meter to set
	 */
	public void setMeter(final Meter meter) {
		this.meter = meter;
	}

	/**
	 * @param reductor
	 *            the reductor to set
	 */
	public void setReductor(final Reductor reductor) {
		this.reductor = reductor;
	}

}
