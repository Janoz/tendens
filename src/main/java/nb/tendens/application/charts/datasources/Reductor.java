package nb.tendens.application.charts.datasources;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

/**
 * Reductor.
 *
 * @author N. Bulthuis
 *
 */
public enum Reductor {

	NONE(1, 1, 1), MINUTE(1, 1, 60), MINUTES_5(1, 5, 60), MINUTES_10(1, 10, 60), MINUTES_15(1, 15, 60), HOUR(1, 60,
			60), DAY(24, 60, 60), MONTH(-1, -1, -1), YEAR(-1, -1, -1);

	/**
	 * The number of hours to round to.
	 */
	private Integer roundToHour;

	/**
	 * The number of minutes to round to.
	 */
	private Integer roundToMinute;

	/**
	 * The number of minutes to round to.
	 */
	private Integer roundToSecond;

	/**
	 * Create a new Reductor with the specified rounding seconds.
	 *
	 * @param roundTo
	 *            the number of seconds to round to.
	 */
	Reductor(final Integer roundToHour, final Integer roundToMinute, final Integer roundToSecond) {
		this.roundToHour = roundToHour;
		this.roundToMinute = roundToMinute;
		this.roundToSecond = roundToSecond;
	}

	/**
	 * Get the Next Reductor Date for the given startdate.
	 * 
	 * @param startDate
	 *            the start date.
	 * @return the next reductordate.
	 */
	public Date getNextReductorDate(final Date startDate) {

		if (this == Reductor.YEAR) {
			return DateUtils.addYears(startDate, 1);
		} else if (this == MONTH) {
			return DateUtils.addMonths(startDate, 1);
		} else {
			final Integer reductorTime = roundToHour * roundToMinute * roundToSecond;
			return DateUtils.addSeconds(startDate, reductorTime);
		}

	}

	/**
	 * @return the roundToHour
	 */
	public Integer getRoundToHour() {
		return roundToHour;
	}

	/**
	 * @return the roundToMinute
	 */
	public Integer getRoundToMinute() {
		return roundToMinute;
	}

	/**
	 * @return the roundToSecond
	 */
	public Integer getRoundToSecond() {
		return roundToSecond;
	}

}
