package nb.tendens.application.charts;

import java.io.Serializable;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

/**
 * PlotLine.
 *
 * @author N. Bulthuis
 *
 */
public class PlotLine implements Serializable {

	/**
	 * Color.
	 */
	private String color;

	/**
	 * Width.
	 */
	private Integer width;

	/**
	 * Z-Index.
	 */
	private Integer zIndex;

	/**
	 * Value.
	 */
	private Double value;

	/**
	 * DashStyle.
	 */
	private DashStyle dashStyle;

	/**
	 * Label.
	 */
	private String label;

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @return the dashStyle
	 */
	public DashStyle getDashStyle() {
		return dashStyle;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the value
	 */
	public Double getValue() {
		return value;
	}

	/**
	 * @return the width
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * @return the zIndex
	 */
	public Integer getZIndex() {
		return zIndex;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(final String color) {
		this.color = color;
	}

	/**
	 * @param dashStyle
	 *            the dashStyle to set
	 */
	public void setDashStyle(final DashStyle dashStyle) {
		this.dashStyle = dashStyle;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(final Double value) {
		this.value = value;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(final Integer width) {
		this.width = width;
	}

	/**
	 * @param zIndex
	 *            the zIndex to set
	 */
	public void setZIndex(final Integer zIndex) {
		this.zIndex = zIndex;
	}

}
