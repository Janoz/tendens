package nb.tendens.application.charts.series;

import java.io.Serializable;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Map;
import java.util.UUID;

import com.google.common.collect.Maps;

import nb.tendens.application.charts.datasources.DataSource;

/**
 * MSerie.
 *
 * @author N. Bulthuis
 *
 */
public abstract class AbstractSerie implements Serializable {

	public enum Key {
		DEFAULT, TOP, BOTTOM, HIGH, LOW;
	}

	/**
	 * Unique id.
	 */
	private final String id = UUID.randomUUID().toString();
	/**
	 * Stacking Type.
	 */
	private StackingType stackingType;

	/**
	 * Stacking Group.
	 */
	private String stackingGroup;

	/**
	 * True if we want to connect the nulls.
	 */
	private boolean connectNulls;

	/**
	 * SerieType.
	 */
	private final SerieType serieType;

	/**
	 * stepType.
	 */
	private StepType stepType;

	/**
	 * Value Suffix.
	 */
	private String valueSuffix;

	/**
	 * Serie Name.
	 */
	private String name;

	/**
	 * Initial Visible.
	 */
	private Boolean visible = Boolean.TRUE;

	/**
	 * Zindex.
	 */
	private Integer zIndex;

	/**
	 * Data Sources.
	 */
	protected final Map<Key, DataSource> dataSources = Maps.newConcurrentMap();

	/**
	 * Color.
	 */
	private String color;

	/**
	 * Serie.
	 *
	 * @param serieType
	 */
	public AbstractSerie(final SerieType serieType) {
		this.serieType = serieType;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @return the dataSources
	 */
	public Map<Key, DataSource> getDataSources() {
		return dataSources;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the serieType
	 */
	public SerieType getSerieType() {
		return serieType;
	}

	/**
	 * @return the stackingGroup
	 */
	public String getStackingGroup() {
		return stackingGroup;
	}

	/**
	 * @return the stackingType
	 */
	public StackingType getStackingType() {
		return stackingType;
	}

	public StepType getStepType() {
		return stepType;
	}

	/**
	 * @return the valueSuffix
	 */
	public String getValueSuffix() {
		return valueSuffix;
	}

	public Boolean getVisible() {
		return visible;
	}

	/**
	 * @return the zIndex
	 */
	public Integer getzIndex() {
		return zIndex;
	}

	public Integer getZIndex() {
		return zIndex;
	}

	public boolean isConnectNulls() {
		return connectNulls;
	}

	/**
	 * Indicates if this serie is valid.
	 *
	 * @return true if valid.
	 */
	public boolean isValid() {

		boolean result = true;

		for (final DataSource ds : dataSources.values()) {

			if (ds.getMeter() == null) {
				result = false;
				break;
			}

		}

		return result;

	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(final String color) {
		this.color = color;
	}

	public void setConnectNulls(final boolean connectNulls) {
		this.connectNulls = connectNulls;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param stackingGroup
	 *            the stackingGroup to set
	 */
	public void setStackingGroup(final String stackingGroup) {
		this.stackingGroup = stackingGroup;
	}

	/**
	 * @param stackingType
	 *            the stackingType to set
	 */
	public void setStackingType(final StackingType stackingType) {
		this.stackingType = stackingType;
	}

	public void setStepType(final StepType stepType) {
		this.stepType = stepType;
	}

	/**
	 * @param valueSuffix
	 *            the valueSuffix to set
	 */
	public void setValueSuffix(final String valueSuffix) {
		this.valueSuffix = valueSuffix;
	}

	public void setVisible(final Boolean visible) {
		this.visible = visible;
	}

	/**
	 * @param zIndex
	 *            the zIndex to set
	 */
	public void setzIndex(final Integer zIndex) {
		this.zIndex = zIndex;
	}

	public void setZIndex(final Integer zIndex) {
		this.zIndex = zIndex;
	}

}
