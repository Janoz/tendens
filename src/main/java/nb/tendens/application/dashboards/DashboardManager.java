package nb.tendens.application.dashboards;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.List;

/**
 * DashboardManager.
 *
 * @author N. Bulthuis
 *
 */
public interface DashboardManager {

	/**
	 * Iterator over all dashboards.
	 *
	 * @param offset
	 *            the offset.
	 * @param count
	 *            the count of dashboards.
	 * @return iterator of dashboards.
	 */
	List<Dashboard> all();

	/**
	 * Get the number of dashboard.
	 *
	 * @return the number of dashboards.
	 */
	long count();

	/**
	 * Get the number of dashboard.
	 *
	 * @param filter
	 *            the filter.
	 * @return the number of dashboards.
	 */
	long count(final String filter);

	/**
	 * Delete a dashboard.
	 *
	 * @param dashboard
	 *            the dashboard to delete.
	 */
	void delete(final Dashboard dashboard);

	/**
	 * Find a dashboard by id.
	 *
	 * @param id
	 *            the id of the dashboard.
	 * @return the dashboard or null.
	 */
	Dashboard findById(final Long id);

	/**
	 * Save or Update a dashboard.
	 *
	 * @param dashboard
	 *            the dashboard to save.
	 */
	void saveOrUpdate(final Dashboard dashboard);

	/**
	 * Iterator over all dashboards.
	 *
	 * @param offset
	 *            the offset.
	 * @param count
	 *            the count of dashboards.
	 * @return iterator of dashboards.
	 */
	List<Dashboard> subList(final String filter, final long offset, final long count);

}
