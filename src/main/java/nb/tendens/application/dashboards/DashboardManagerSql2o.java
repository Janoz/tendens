package nb.tendens.application.dashboards;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import com.google.common.collect.Lists;

/**
 * DashboardManagerSql2o.
 *
 * @author N. Bulthuis
 *
 */
public class DashboardManagerSql2o implements DashboardManager {

	/**
	 * Sql2o.
	 */
	private final Sql2o sql2o;

	/**
	 * Cosntructor.
	 *
	 * @param sql2o
	 *            the sql2o.
	 */
	public DashboardManagerSql2o(final Sql2o sql2o) {
		this.sql2o = sql2o;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.dashboards.ChartManager#iterate(long, long)
	 */
	@Override
	public final List<Dashboard> all() {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "id "
				+ ", config "
				+ "FROM "
				+ "	dashboards "
				+ " ORDER BY name "
				+ ";";
			// @formatter:on

			final List<DashboardConverter> pos = cn.createQuery(sql).executeAndFetch(DashboardConverter.class);

			final List<Dashboard> dashboards = Lists.newArrayList();
			for (final DashboardConverter po : pos) {
				dashboards.add(po.toDashboard());
			}

			return dashboards;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long count() {

		try (Connection cn = sql2o.open()) {
			return cn.createQuery("SELECT COUNT(*) FROM dashboards").executeScalar(Long.class);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long count(final String filter) {

		if (StringUtils.isNoneBlank(filter)) {

			try (Connection cn = sql2o.open()) {
				return cn.createQuery("SELECT COUNT(*) FROM dashboards WHERE name LIKE :filter")
						.addParameter("filter", "%" + filter + "%").executeScalar(Long.class);
			}
		} else {
			return count();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void delete(final Dashboard dashboard) {

		try (Connection cn = sql2o.beginTransaction()) {

			final Query q = cn.createQuery("DELETE FROM dashboards WHERE id = :id");
			q.addParameter("id", dashboard.getId());
			q.executeUpdate();

			cn.commit();
		}
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public Dashboard findById(final Long id) {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "id "
				+ ", config "
				+ "FROM "
				+ "	dashboards "
				+ "WHERE id = :id "
				+ ";";
			// @formatter:on

			final DashboardConverter po = cn.createQuery(sql).addParameter("id", id)
					.executeAndFetchFirst(DashboardConverter.class);

			if (po != null) {
				return po.toDashboard();
			} else {
				return null;
			}

		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void saveOrUpdate(final Dashboard dashboard) {

		try (Connection cn = sql2o.beginTransaction()) {

			Query q = null;

			if (dashboard.getId() != null) {

				q = cn.createQuery("UPDATE dashboards SET config = :config, name = :name WHERE id = :id");

			} else {

				q = cn.createQuery("INSERT INTO dashboards (id, name, config) VALUES (:id, :name, :config)");

			}
			q.addParameter("id", dashboard.getId());
			q.addParameter("name", dashboard.getName());
			q.addParameter("config", new DashboardConverter(dashboard).getConfig());
			q.executeUpdate();

			cn.commit();

		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.dashboards.ChartManager#subList(long, long)
	 */
	@Override
	public List<Dashboard> subList(final String filter, final long offset, final long count) {
		final List<Dashboard> result = withFilter(filter);

		return result.subList((int) offset, Math.min((int) (offset + count), result.size()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.dashboards.ChartManager#iterate(long, long)
	 */
	public final List<Dashboard> withFilter(final String filter) {

		if (StringUtils.isNoneBlank(filter)) {

			try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "id "
				+ ", config "
				+ "FROM "
				+ "	dashboards "
				+ " WHERE name LIKE :filter "
				+ " ORDER BY name "
				+ ";";
			// @formatter:on

				final List<DashboardConverter> pos = cn.createQuery(sql).addParameter("filter", "%" + filter + "%")
						.executeAndFetch(DashboardConverter.class);

				final List<Dashboard> dashboards = Lists.newArrayList();
				for (final DashboardConverter po : pos) {
					dashboards.add(po.toDashboard());
				}

				return dashboards;

			}
		} else {
			return all();
		}
	}

}
