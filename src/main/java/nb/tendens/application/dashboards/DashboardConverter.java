package nb.tendens.application.dashboards;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.Serializable;

import com.owlike.genson.Genson;

import nb.tendens.application.shared.AbstractPersistentObject;
import nb.tendens.infrastructure.utils.JsonUtilities;

/**
 * DashboardConverter.
 *
 * @author N. Bulthuis
 *
 */
public class DashboardConverter extends AbstractPersistentObject implements Serializable {

	/**
	 * Serial ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Chart Config.
	 */
	private final String config;

	/**
	 * Construct a new PO from the specified chart configuration.
	 *
	 * @param chartConfiguration
	 */
	public DashboardConverter(final Dashboard dashboard) {
		final Genson genson = JsonUtilities.newRuntimetypeGenson();

		setId(dashboard.getId());
		config = genson.serialize(dashboard);

	}

	/**
	 * @return the config
	 */
	public String getConfig() {

		return config;
	}

	/**
	 * Convert a PO to a Dashboard.
	 *
	 * @return dashboard.
	 */
	public final Dashboard toDashboard() {
		final Genson genson = JsonUtilities.newRuntimetypeGenson();
		final Dashboard d = genson.deserialize(config, Dashboard.class);
		d.setId(getId());

		return d;
	}

}
