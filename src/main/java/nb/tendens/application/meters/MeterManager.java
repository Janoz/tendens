package nb.tendens.application.meters;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.List;

/**
 * MeterManager.
 *
 * @author N. Bulthuis
 *
 */
public interface MeterManager {

	/**
	 * Get all meters.
	 *
	 * @return all meters.
	 */
	List<Meter> all();

	/**
	 * Get the number of meters.
	 *
	 * @return the number of meters.
	 */
	long count();

	/**
	 * Get the number of chart.
	 *
	 * @return the number of chart.
	 */
	long count(final String filter);

	/**
	 * Delete a meter.
	 *
	 * @param meter
	 *            the meter to delete.
	 */
	void delete(final Meter meter);

	/**
	 * Find a meter by id.
	 *
	 * @param id
	 *            the id of the meter.
	 * @return the meter or null.
	 */
	Meter findById(final Long id);

	/**
	 * Find MeterInfo for the specified id.
	 * 
	 * @param id
	 *            the meter id.
	 * @return the meterinfo or null if the meter does not exist.
	 */
	MeterInfo findMeterInfoById(final Long id);

	/**
	 * Save or Update a meter.
	 *
	 * @param meter
	 *            the meter to save.
	 */
	void saveOrUpdate(final Meter meter);

	/**
	 * Iterator over all charts.
	 *
	 * @param offset
	 *            the offset.
	 * @param count
	 *            the count of charts.
	 * @return iterator of charts.
	 */
	List<Meter> subList(final String filter, final long offset, final long count);
}
