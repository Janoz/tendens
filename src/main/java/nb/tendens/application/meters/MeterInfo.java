package nb.tendens.application.meters;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;

import nb.tendens.application.shared.AbstractPersistentObject;

/**
 * Meter.
 *
 * @author N. Bulthuis
 *
 */
public class MeterInfo extends AbstractPersistentObject implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Name of the meter.
	 */
	private String name;

	/**
	 * Last Metric.
	 */
	private Date lastMetric;

	/**
	 * Number of metrics.
	 */
	private Long metricCount;

	/**
	 * First Metric.
	 */
	private Date firstMetric;

	/**
	 * @return the firstMetric
	 */
	public Date getFirstMetric() {
		return firstMetric;
	}

	/**
	 * @return the lastMetric
	 */
	public Date getLastMetric() {
		return lastMetric;
	}

	/**
	 * @return the metricCount
	 */
	public Long getMetricCount() {
		return metricCount;
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @param firstMetric
	 *            the firstMetric to set
	 */
	public void setFirstMetric(final Date firstMetric) {
		this.firstMetric = firstMetric;
	}

	/**
	 * @param lastMetric
	 *            the lastMetric to set
	 */
	public void setLastMetric(final Date lastMetric) {
		this.lastMetric = lastMetric;
	}

	/**
	 * @param metricCount
	 *            the metricCount to set
	 */
	public void setMetricCount(final Long metricCount) {
		this.metricCount = metricCount;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}
}
