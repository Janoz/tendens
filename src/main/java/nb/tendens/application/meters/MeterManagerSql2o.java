package nb.tendens.application.meters;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

/**
 * MeterManager.
 *
 * @author N. Bulthuis
 *
 */
public class MeterManagerSql2o implements MeterManager {

	/**
	 * Sql2o.
	 */
	private final Sql2o sql2o;

	/**
	 * Cosntructor.
	 *
	 * @param sql2o
	 *            the sql2o.
	 */
	public MeterManagerSql2o(final Sql2o sql2o) {
		this.sql2o = sql2o;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.meters.MeterManager#all()
	 */
	@Override
	public final List<Meter> all() {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "m.id "
				+ ",	m.name "
				+ "FROM "
				+ "	meters m "
				+ "ORDER BY m.name, m.id "
				+ ";";
			// @formatter:on
			return cn.createQuery(sql).executeAndFetch(Meter.class);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long count() {

		try (Connection cn = sql2o.open()) {
			return cn.createQuery("SELECT COUNT(*) FROM meters").executeScalar(Long.class);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final long count(final String filter) {

		if (StringUtils.isNoneBlank(filter)) {

			try (Connection cn = sql2o.open()) {
				return cn.createQuery("SELECT COUNT(*) FROM meters WHERE name LIKE :filter")
						.addParameter("filter", "%" + filter + "%").executeScalar(Long.class);
			}
		} else {
			return count();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void delete(final Meter meter) {

		try (Connection cn = sql2o.beginTransaction()) {

			final Query q = cn.createQuery("DELETE FROM meters WHERE id = :id");
			q.addParameter("id", meter.getId());
			q.executeUpdate();

			cn.commit();
		}
	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public Meter findById(final Long id) {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
				+ "m.id "
				+ ",	m.name "
				+ "FROM "
				+ "	meters m "
				+ "WHERE m.id = :id "
				+ ";";
			// @formatter:on

			return cn.createQuery(sql).addParameter("id", id).executeAndFetchFirst(Meter.class);
		}

	}

	/**
	 * {@inheritDoc}.
	 */
	@Override
	public MeterInfo findMeterInfoById(final Long id) {

		try (Connection cn = sql2o.open()) {

			// @formatter:off
			final String sql = "SELECT "
					+ "m.id AS id "
					+ ",	m.name AS name "
					+ ",	COUNT(e.id) AS metricCount "
					+ ",	MIN(e.datetime) AS firstMetric "
					+ ",	MAX(e.datetime) AS lastMetric "
					+ "FROM meters m "
					+ "LEFT JOIN metrics e ON m.id = e.meter_id "
					+ "WHERE "
					+ "	m.id = :id "
					+ "GROUP BY "
					+ "	m.id, m.name "
					+ ";";
			// @formatter:on

			return cn.createQuery(sql).addParameter("id", id).executeAndFetchFirst(MeterInfo.class);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void saveOrUpdate(final Meter meter) {

		try (Connection cn = sql2o.beginTransaction()) {

			Query q = null;

			if (meter.getId() != null) {

				q = cn.createQuery("UPDATE meters SET name = :name WHERE id = :id");
				q.addParameter("id", meter.getId());
				q.addParameter("name", meter.getName());
				q.executeUpdate();

			} else {

				q = cn.createQuery("INSERT INTO meters (name) VALUES (:name)", true);
				q.addParameter("name", meter.getName());
				final Long newKey = q.executeUpdate().getKey(Long.class);
				meter.setId(newKey);
			}

			cn.commit();

		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.charts.ChartManager#subList(long, long)
	 */
	@Override
	public List<Meter> subList(final String filter, final long offset, final long count) {

		return withFilter(filter, offset, offset + count);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see nb.tendens.application.charts.ChartManager#iterate(long, long)
	 */
	public final List<Meter> withFilter(final String filter, final long offset, final long count) {

		if (StringUtils.isNoneBlank(filter)) {

			try (Connection cn = sql2o.open()) {

			// @formatter:off
				final String sql = "SELECT "
						+ "m.id "
						+ ",	m.name "
						+ "FROM "
						+ "	meters m "
						+ " WHERE name LIKE :filter "
						+ "ORDER BY m.name, m.id "
						+ "LIMIT :offset, :count"
						+ ";";
			// @formatter:on

				return cn.createQuery(sql).addParameter("offset", offset).addParameter("count", count)
						.addParameter("filter", "%" + filter + "%").executeAndFetch(Meter.class);

			}
		} else {
			return all();
		}
	}

}
