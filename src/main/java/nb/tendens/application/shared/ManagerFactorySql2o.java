package nb.tendens.application.shared;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import org.sql2o.Sql2o;

import nb.tendens.application.charts.ChartManager;
import nb.tendens.application.charts.ChartManagerSql2o;
import nb.tendens.application.dashboards.DashboardManager;
import nb.tendens.application.dashboards.DashboardManagerSql2o;
import nb.tendens.application.meters.MeterManager;
import nb.tendens.application.meters.MeterManagerSql2o;
import nb.tendens.application.metrics.MetricManager;
import nb.tendens.application.metrics.MetricManagerSql2o;
import nb.tendens.application.schemachanges.SchemaChanges;
import nb.tendens.infrastructure.configuration.Configuration;

/**
 * Manager Factory.
 *
 * @author Nicky Bulthuis
 */
public class ManagerFactorySql2o implements ManagerFactory {

	private final Sql2o sql2o;

	public ManagerFactorySql2o(final Configuration configuration) {

		sql2o = new Sql2o(configuration.getDataSource());

		final SchemaChanges sc = new SchemaChanges(sql2o);
		sc.performPendingSchemaChanges();

	}

	/**
	 * Get the Chart Manager.
	 *
	 * @return the Chart Manager.
	 */
	@Override
	public ChartManager getChartManager() {
		return new ChartManagerSql2o(sql2o);
	}

	/**
	 * Get the Dashboard Manager.
	 *
	 * @return the Dashboard Manager.
	 */
	@Override
	public DashboardManager getDashboardManager() {
		return new DashboardManagerSql2o(sql2o);
	}

	/**
	 * Get the Meter Manager.
	 *
	 * @return the meter Manager.
	 */
	@Override
	public MeterManager getMeterManager() {
		return new MeterManagerSql2o(sql2o);
	}

	/**
	 * Get the Metric Manager.
	 *
	 * @return the metric Manager.
	 */
	@Override
	public MetricManager getMetricManager() {
		return new MetricManagerSql2o(sql2o);
	}

}
