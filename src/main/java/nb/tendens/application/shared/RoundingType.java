package nb.tendens.application.shared;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

/**
 * RoundingType.
 *
 * @author N. Bulthuis
 *
 */
public enum RoundingType {

	/**
	 * Round up.
	 */
	ROUND_UP, /**
				 * Round.
				 */
	ROUND_AUTO, /**
				 * Round down.
				 */
	ROUND_DOWN;

	/**
	 * One second in ms.
	 */
	private static final Long DURATION_1_SECOND_IN_MS = 1_000L;

	/**
	 * Perform rounding on a DateTime.
	 *
	 * @param type
	 *            the type of rounding.
	 * @param datetime
	 *            the date time.
	 * @param roundTo
	 *            the number of seconds to round to.
	 * @return the rounded DateTime.
	 */
	public static Date roundDateTime(final RoundingType type, final Date datetime, final Integer roundTo) {

		Date dt = DateUtils.truncate(datetime, Calendar.SECOND);

		final long mod = dt.getTime() / DURATION_1_SECOND_IN_MS % roundTo;
		RoundingType modType = type;

		if (type == ROUND_AUTO) {
			modType = mod >= roundTo / 2 ? ROUND_UP : ROUND_DOWN;
		}

		if (mod > 0) {
			if (modType == ROUND_UP) {
				dt = DateUtils.addSeconds(dt, (int) (roundTo - mod));
			} else {
				dt = DateUtils.addSeconds(dt, (int) (mod * -1));
			}
		}
		return dt;
	}

}
