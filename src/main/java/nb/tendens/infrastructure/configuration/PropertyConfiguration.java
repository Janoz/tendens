package nb.tendens.infrastructure.configuration;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;

import com.mchange.v2.c3p0.DataSources;

public class PropertyConfiguration implements Configuration {

	/**
	 * Key for the configuration file.
	 */
	public static final String KEY_CONFIGURATION_FILE = "tendensConfiguration";

	/**
	 * Default Timezone.
	 */
	public static final String DEFAULT_TIMEZONE = "Europe/Amsterdam";

	/**
	 * Config.
	 */
	private final Properties config = new Properties();

	/**
	 * Construct a new Configuration.
	 *
	 * @param configurationFile
	 *            the confiugration file.
	 */
	public PropertyConfiguration(final File configurationFile) {

		try (InputStream in = FileUtils.openInputStream(configurationFile)) {

			config.load(FileUtils.openInputStream(configurationFile));

		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * nb.tendens.infrastructure.configuration.IConfiguration#getDataSource()
	 */
	@Override
	public DataSource getDataSource() {

		final Properties dsProperties = new Properties();
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		} catch (final ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		final String url = config.getProperty("jdbc.url", "jdbc:mariadb://localhost/tendens");
		dsProperties.setProperty("user", config.getProperty("jdbc.user", "tendens"));
		dsProperties.setProperty("password", config.getProperty("jdbc.password", "tendenspass"));

		try {
			return DataSources.unpooledDataSource(url, dsProperties);
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * nb.tendens.infrastructure.configuration.IConfiguration#getDefaultTimeZone
	 * ()
	 */
	@Override
	public String getDefaultTimeZone() {
		return config.getProperty("default.timezone", DEFAULT_TIMEZONE);
	}

}
