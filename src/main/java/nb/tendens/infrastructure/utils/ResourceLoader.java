package nb.tendens.infrastructure.utils;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */


import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

/**
 * ResourceLoader.
 *
 * @author N. Bulthuis
 *
 */
public final class ResourceLoader {

	/**
	 * Load the Resource Quietly, ignoring any exceptions.
	 * 
	 * @param resourceName
	 *            the name of the resource.
	 * @return the string or null if the resource was not found.
	 */
	public static String toStringQuietly(final String resourceName) {

		String result = null;

		try {

			try (InputStream resourceStream = ResourceLoader.class.getClassLoader().getResourceAsStream(resourceName)) {

				result = IOUtils.toString(resourceStream);
			}
		} catch (final IOException e) {
			// ignore
		}

		return result;
	}

	/**
	 * Hidden Constructor.
	 */
	private ResourceLoader() {

	}

}
