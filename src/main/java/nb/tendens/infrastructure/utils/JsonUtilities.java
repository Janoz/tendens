package nb.tendens.infrastructure.utils;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

/**
 * GsonUtilities.
 *
 * @author N. Bulthuis
 *
 */
public final class JsonUtilities {

	/**
	 * Create a new GsonBuilder.
	 *
	 * @return a gson builder.
	 */
	private static GensonBuilder newBuilder() {
		final GensonBuilder builder = new GensonBuilder();
		// builder.useDateFormat(new
		// SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZ"));
		// builder.useDateAsTimestamp(true);
		builder.useClassMetadata(true);
		builder.useIndentation(true);
		return builder;
	}

	/**
	 * Build a new Genson.
	 *
	 * @return the genson.
	 */
	public static Genson newGenson() {

		final GensonBuilder gensonBuilder = newBuilder();
		return gensonBuilder.create();
	}

	/**
	 * Build a new Genson using Class Runtime Types.
	 *
	 * @return
	 */
	public static Genson newRuntimetypeGenson() {

		final GensonBuilder gensonBuilder = newBuilder();
		gensonBuilder.useRuntimeType(true);
		return gensonBuilder.create();

	}

	/**
	 * Private Constructor.
	 */
	private JsonUtilities() {

	}

}
