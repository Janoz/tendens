package nb.tendens.infrastructure.utils;

/*
 * #%L
 * zipapi
 * %%
 * Copyright (C) 2015 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */

import java.util.Date;
import java.util.List;

import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;

/**
 * Interval.
 *
 * @author N. Bulthuis
 *
 */
public final class Interval {

	/**
	 * Parse a date pattern with natty.
	 *
	 * @param pattern
	 *            the pattern.
	 * @return the date or null.
	 */
	public static Date parse(final String pattern) {

		return parse(pattern, null);

	}

	/**
	 * Parse a date pattern with natty.
	 *
	 * @param pattern
	 *            the pattern.
	 * @param defaultDate
	 *            the date if natty could not parse it.
	 * @return the date or the default date.
	 */

	public static Date parse(final String pattern, final Date defaultDate) {

		Date result = defaultDate;

		if (pattern != null) {

			final Parser nattyParser = new Parser();
			final List<DateGroup> dateGroups = nattyParser.parse(pattern);

			if (!dateGroups.isEmpty()) {

				final List<Date> dates = dateGroups.get(0).getDates();

				if (!dates.isEmpty()) {
					result = dates.get(0);
				}
			}
		}
		return result;

	}

	/**
	 * Hidden Constructor.
	 */
	private Interval() {

	}

}
