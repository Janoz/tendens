package nb.tendens.infrastructure.utils;

/*
 * #%L
 * tendens
 * %%
 * Copyright (C) 2015 - 2016 N. Bulthuis
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */


import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Pair<T> {
	private static class PairWise<T> implements Iterable<Pair<T>>, Iterator<Pair<T>> {

		final Iterator<T> iterator;

		PairWise(final Collection<T> collection) {
			super();
			this.iterator = collection.iterator();
		}

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public Iterator<Pair<T>> iterator() {
			return this;
		}

		@Override
		public Pair<T> next() {
			T first = null;
			T second = null;
			if (iterator.hasNext()) {
				first = iterator.next();
			} else {
				throw new NoSuchElementException();
			}
			if (iterator.hasNext()) {
				second = iterator.next();
			}
			return new Pair<T>(first, second);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	public static <T> Iterable<Pair<T>> over(final Collection<T> collection) {
		return new PairWise<T>(collection);
	}

	public T first;

	public T second;

	public Pair(final T first, final T second) {
		this.first = first;
		this.second = second;
	}
}
