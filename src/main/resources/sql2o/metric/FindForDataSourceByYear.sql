SELECT
	DATE_SUB(DATE_SUB(DATE_SUB(DATE_SUB(DATE_SUB(datetime, INTERVAL MONTH(datetime)-1 MONTH ), 
			INTERVAL DAYOFMONTH(datetime)-1 DAY ), 
         INTERVAL HOUR(datetime) HOUR), 
         INTERVAL MINUTE(datetime) MINUTE), 
         INTERVAL SECOND(datetime) SECOND)  as datetime
,	MIN(value) AS valueMin
,	MAX(value) AS valueMax
,    (MAX(value) - MIN(value)) AS valueChange
,	AVG(value) AS valueAvg
,	SUM(value) AS valueSum
, 	:aggregator AS aggregator
FROM
metrics
WHERE meter_id = :meter_id
AND datetime BETWEEN :from AND :until
GROUP BY 1
ORDER BY 1 ASC