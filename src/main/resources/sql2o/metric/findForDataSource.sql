SELECT
	DATE_SUB(DATE_SUB(DATE_SUB(datetime, 
         INTERVAL MOD(HOUR(datetime), :roundToHour) HOUR), 
         INTERVAL MOD(MINUTE(datetime), :roundToMinute) MINUTE), 
         INTERVAL MOD(SECOND(datetime), :roundToSecond) SECOND) as datetime
,	MIN(value) AS valueMin
,	MAX(value) AS valueMax
,   (MAX(value) - MIN(value)) AS valueChange
,	AVG(value) AS valueAvg
,	SUM(value) AS valueSum
, 	:aggregator AS aggregator
FROM
	metrics
WHERE meter_id = :meter_id
AND datetime BETWEEN :from AND :until
GROUP BY 1
ORDER BY 1 ASC
